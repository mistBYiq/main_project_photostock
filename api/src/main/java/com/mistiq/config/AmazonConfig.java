package com.mistiq.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Configuration
@ConfigurationProperties("amazon.images")
public class AmazonConfig {

    // @Value("${}")
    private String s3;

    private String accessKeyId;

    private String secretKey;

    private String bucket;

    private String serverUrlOrigin;

    private String serverUrlPreview;

    private String userFolder;

    private String region;

    @Bean
    public S3Client s3Client(AmazonConfig amazonConfiguration) {
        return S3Client
                .builder()
                .region(Region.of(amazonConfiguration.getRegion()))
                .credentialsProvider(() ->
                        AwsBasicCredentials.create(
                                amazonConfiguration.getAccessKeyId(),
                                amazonConfiguration.getSecretKey()
                        ))
                .build();
    }
}
