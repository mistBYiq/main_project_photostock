package com.mistiq.security.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AuthRequest {

    @ApiModelProperty(dataType = "string", example = "uniqUserLogin", notes = "login")
    private String login;

    @ApiModelProperty(dataType = "string", example = "password", notes = "password")
    private String password;
}
