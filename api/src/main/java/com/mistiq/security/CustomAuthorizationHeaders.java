package com.mistiq.security;

public interface CustomAuthorizationHeaders {
    String AUTH_HEADER = "X-Auth-Token";
}
