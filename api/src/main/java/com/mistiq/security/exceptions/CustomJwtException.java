package com.mistiq.security.exceptions;

public class CustomJwtException extends RuntimeException {

    public CustomJwtException() {
        super();
    }

    public CustomJwtException(String message) {
        super(message);
    }
}
