package com.mistiq.security.service;

import com.mistiq.domain.RoleName;
import com.mistiq.domain.entity.Role;
import com.mistiq.domain.entity.User;
import com.mistiq.repository.impl.RoleRepository;
import com.mistiq.repository.impl.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    // in this method, we check by the passed name, pull the real user from the base,
    // with his password and roles, for further verification
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Optional<User> searchResult = userRepository.findByCredentialLogin(username);
            if (searchResult.isPresent()) {
                User user = searchResult.get();
                return new org.springframework.security.core.userdetails.User(
                        user.getCredential().getLogin(),
                        user.getCredential().getPassword(),
                        AuthorityUtils.commaSeparatedStringToAuthorityList(roleRepository.findRolesByUserId(user.getId())
                                .stream()
                                .map(Role::getName)
                                .map(RoleName::name)
                                .collect(Collectors.joining(",")))
                );
            } else {
                throw new UsernameNotFoundException(String.format("No user found with login '%s'.", username));
            }
        } catch (Exception e) {
            throw new UsernameNotFoundException("User with this login not found");
        }
    }
}
