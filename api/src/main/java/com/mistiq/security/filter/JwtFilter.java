package com.mistiq.security.filter;

import com.mistiq.security.CustomAuthorizationHeaders;
import com.mistiq.security.exceptions.CustomJwtException;
import com.mistiq.security.util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {
    private static final Logger log = Logger.getLogger(JwtFilter.class);

    private final JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        final String authorizationHeader = request.getHeader(CustomAuthorizationHeaders.AUTH_HEADER);

        String username = null;
        String jwt = null;

        if (authorizationHeader != null) {
            try {
                jwt = authorizationHeader;
                // if the signature does not match the computed one, then - SignatureException
                // if the signature is incorrect (not parsed) then - MalformedJwtException
                // if the signature has expired, then - ExpiredJwtException
                username = jwtUtil.getUsernameFromToken(jwt);
            } catch (Exception ex) {
                if (ex instanceof SignatureException) {
                    log.error(ex.getMessage(), ex);
                    log.info(ex.getMessage(), ex);
                    throw new CustomJwtException("Signature Mismatch");

                } else if (ex instanceof MalformedJwtException) {
                    log.error(ex.getMessage(), ex);
                    log.info(ex.getMessage(), ex);
                    throw new CustomJwtException("Malformed JWT");

                } else if (ex instanceof ExpiredJwtException) {
                    log.error(ex.getMessage(), ex);
                    log.info(ex.getMessage(), ex);
                    throw new CustomJwtException("Expired JWT");
                }
            }
        }


        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            String commaSeparatedListOfAuthorities = jwtUtil.getAuthoritiesFromToken(jwt);
            List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(commaSeparatedListOfAuthorities);
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(
                            username, null, authorities);

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        }
        chain.doFilter(request, response);
    }
}
