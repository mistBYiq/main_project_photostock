package com.mistiq.security.controller;

import com.mistiq.controller.request.create.UserCreateRequest;
import com.mistiq.domain.entity.User;
import com.mistiq.security.service.SendMailService;
import com.mistiq.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/registration")
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;
    private final SendMailService sendMailService;
    private final ConversionService conversionService;
    private final PasswordEncoder passwordEncoder;

    @ApiOperation(value = "Endpoint for registration user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "OK. Successful creation user"),
            @ApiResponse(code = 400, message = "Unprocessable Entity. Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server ERROR")
    })
    @PostMapping
    public ResponseEntity<Map<String, Object>> registration(@Valid @RequestBody UserCreateRequest request) {

        User user = conversionService.convert(request, User.class);
        if (user != null) {
            User savedUser = userService.save(user);

            Map<String, Object> result = new HashMap<>();

            result.put("id", savedUser.getId());
            result.put("login", savedUser.getCredential().getLogin());
            result.put("password", savedUser.getCredential().getPassword());

            //sendWelcomeMessage(savedUser.getEmail(), savedUser.getFullName());

            return new ResponseEntity<>(result, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
    }

    public void sendWelcomeMessage(String email, String username) {

        String subject = "Photo Stock Registration";
        String message = "Hello, " + username + "!\n Congratulation! \n You have successfully registered into Photo Stock!";
        // Send Message
        this.sendMailService.send(email, subject, message);
    }
}
