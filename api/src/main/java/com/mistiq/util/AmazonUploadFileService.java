package com.mistiq.util;

public interface AmazonUploadFileService {
    String uploadFile(byte[] image, Long authorId);

//    void uploadFile(MultipartFile multipartFile);
//
//    byte[] downloadFile(String keyName);
//
//    void deleteFile(String keyName);
}
