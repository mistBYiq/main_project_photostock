package com.mistiq.util;

import com.mistiq.controller.exception.ValidateException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class MyConverters {

    public Date convertStringToDate(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Date birthDay = null;
        try {
            birthDay = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new ValidateException("Exception The date is not in the correct format");
        }
        return birthDay;
    }
}
