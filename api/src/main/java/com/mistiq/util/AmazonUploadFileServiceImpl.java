package com.mistiq.util;

import com.mistiq.config.AmazonConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.util.UUID;

//@Slf4j
@Service
@RequiredArgsConstructor
public class AmazonUploadFileServiceImpl implements AmazonUploadFileService {

    private final S3Client s3Client;

    private final AmazonConfig amazonS3Config;

    @Override
    public String uploadFile(byte[] imageBytes, Long authorId) {
        String imageUUID = UUID.randomUUID().toString();

        s3Client.putObject(
                PutObjectRequest.builder()
                        .bucket(amazonS3Config.getBucket())
                        .contentType("image/jpeg")
                        .contentLength((long) imageBytes.length)
                        .acl(ObjectCannedACL.PUBLIC_READ)
                        .key(String.format("%s/%s/%s.jpg", amazonS3Config.getUserFolder(), authorId, imageUUID))
                        .build(),
                RequestBody.fromBytes(imageBytes)
        );

        return String.format("%s/%s/%s/%s/%s.jpg",
                amazonS3Config.getServerUrlOrigin(),
                amazonS3Config.getBucket(),
                amazonS3Config.getUserFolder(),
                authorId, imageUUID);
    }

//
//    @Value("${amazone.images.bucket}")
//    private String bucketName;
//
//    @Override
//    // @Async annotation ensures that the method is executed in a different background thread
//    // but not consume the main thread.
//    @Async
//    public void uploadFile(final MultipartFile multipartFile) {
//
//        try {
//            final File file = convertMultiPartFileToFile(multipartFile);
//            uploadFileToS3Bucket(bucketName, file);
//
//            file.delete();	// To remove the file locally created in the project folder.
//        } catch (final AmazonServiceException ex) {
//
//        }
//    }
//
//    private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
//        final File file = new File(multipartFile.getOriginalFilename());
//        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
//            outputStream.write(multipartFile.getBytes());
//        } catch (final IOException ex) {
//     //
//        }
//        return file;
//    }
//
//    private void uploadFileToS3Bucket(final String bucketName, final File file) {
//        final String uniqueFileName = LocalDateTime.now() + "_" + file.getName();
//
//        final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uniqueFileName, file);
//        s3Client.putObject(putObjectRequest);
//    }
//
//    @Override
//    // @Async annotation ensures that the method is executed in a different background thread
//    // but not consume the main thread.
//    @Async
//    public byte[] downloadFile(final String keyName) {
//        byte[] content = null;
//        final S3Object s3Object = s3Client.getObject(bucketName, keyName);
//        final S3ObjectInputStream stream = s3Object.getObjectContent();
//        try {
//            content = IOUtils.toByteArray(stream);
//
//            s3Object.close();
//        } catch(final IOException ex) {
//          //
//        }
//        return content;
//    }
//
//    @Override
//    // @Async annotation ensures that the method is executed in a different background thread
//    // but not consume the main thread.
//    @Async
//    public void deleteFile(final String keyName) {
//
//        final DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucketName, keyName);
//        s3Client.deleteObject(deleteObjectRequest);
//    }
}
