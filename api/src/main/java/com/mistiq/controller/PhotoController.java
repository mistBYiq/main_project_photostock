package com.mistiq.controller;

import com.mistiq.domain.entity.Photo;
import com.mistiq.service.PhotoService;
import com.mistiq.util.AmazonUploadFileService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/rest/photos/{photoId}/upload")
@RequiredArgsConstructor
public class PhotoController {

    private final PhotoService photoService;

    private final AmazonUploadFileService amazonUploadFileService;

    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful upload order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @PostMapping("/{photoId}")
    public ResponseEntity<Map<Object, Object>> uploadUserPhoto(@PathVariable Long photoId,
                                                               @NonNull @RequestBody MultipartFile file) throws IOException {
// check for image
        if (file.getContentType() != null && !file.getContentType().toLowerCase().startsWith("image")) {
            throw new MultipartException("not image");
        }

        Photo photo = photoService.findPhotoById(photoId);
        byte[] imageBytes = file.getBytes();
        String imageLink = amazonUploadFileService.uploadFile(imageBytes, photoId);
        photo.setUrl(imageLink);

        // todo: convert image size, add watermark, save convert image link in preview

        photoService.save(photo);

        return new ResponseEntity<>(Collections.singletonMap("imageLink", imageLink), HttpStatus.CREATED);
    }



//    @PostMapping(value= "/upload")
//    public ResponseEntity<String> uploadFile(@RequestPart(value= "file") final MultipartFile multipartFile) {
//        amazonUploadFileService.uploadFile(multipartFile);
//        final String response = "[" + multipartFile.getOriginalFilename() + "] uploaded successfully.";
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }
//
//    @GetMapping(value= "/download")
//    public ResponseEntity<ByteArrayResource> downloadFile(@RequestParam(value= "fileName") final String keyName) {
//        final byte[] data = amazonUploadFileService.downloadFile(keyName);
//        final ByteArrayResource resource = new ByteArrayResource(data);
//        return ResponseEntity
//                .ok()
//                .contentLength(data.length)
//                .header("Content-type", "application/octet-stream")
//                .header("Content-disposition", "attachment; filename=\"" + keyName + "\"")
//                .body(resource);
//    }
//
//    @DeleteMapping(value= "/delete")
//    public ResponseEntity<String> deleteFile(@RequestParam(value= "fileName") final String keyName) {
//        amazonUploadFileService.deleteFile(keyName);
//        final String response = "[" + keyName + "] deleted successfully.";
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }

}
