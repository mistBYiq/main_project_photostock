package com.mistiq.controller.responces;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {

    private String errorMessage;

    private String debugMessage;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> errors;

    public ApiError(String errorMessage, String debugMessage) {
        this.errorMessage = errorMessage;
        this.debugMessage = debugMessage;
    }
}