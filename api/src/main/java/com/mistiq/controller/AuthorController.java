package com.mistiq.controller;

import com.mistiq.controller.exception.ReAddException;
import com.mistiq.controller.exception.ValidateException;
import com.mistiq.controller.request.create.CategoryCreateRequest;
import com.mistiq.controller.request.create.KeywordCreateRequest;
import com.mistiq.controller.request.create.LicenseCreateRequest;
import com.mistiq.controller.request.create.PhotoCreateRequest;
import com.mistiq.domain.CategoryName;
import com.mistiq.domain.LicenseType;
import com.mistiq.domain.RoleName;
import com.mistiq.domain.entity.Category;
import com.mistiq.domain.entity.Keyword;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Photo;
import com.mistiq.domain.entity.Role;
import com.mistiq.service.CategoryService;
import com.mistiq.service.KeywordService;
import com.mistiq.service.LicenseService;
import com.mistiq.service.LinkKeywordPhotoService;
import com.mistiq.service.OrderService;
import com.mistiq.service.PhotoService;
import com.mistiq.service.RoleService;
import com.mistiq.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/authors/{authorId}")
@RequiredArgsConstructor
public class AuthorController {

    public final UserService userService;

    public final RoleService roleService;

    public final CategoryService categoryService;

    public final LicenseService licenseService;

    public final KeywordService keywordService;

    public final PhotoService photoService;

    public final OrderService orderService;

    public final LinkKeywordPhotoService linkKeywordPhotoService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Change role to Client")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful change role"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "User id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @PatchMapping("/role")
    public ResponseEntity<Role> changeRoleToClient(@PathVariable("authorId") Long userId) {
        userService.findUserById(userId);
        Role role = roleService.findRoleByUserId(userId);

        if (!roleService.isClient(userId)) {
            role.setName(RoleName.ROLE_CLIENT);
            return new ResponseEntity<>(roleService.save(role), HttpStatus.OK);
        }

        return new ResponseEntity<>(role, HttpStatus.NOT_MODIFIED);
    }


    @ApiOperation(value = "Delete author by id (not from the database but mark \"isDeleted\" = true)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleted author"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @Transactional
    @PutMapping("/delete-profile")
    public ResponseEntity<Void> deleteAuthor(@PathVariable("authorId") Long authorId) {
        userService.findUserById(authorId);

        if (roleService.isAuthor(authorId)) {
            userService.deleteAuthor(authorId);
            photoService.deleteAuthorPhotos(authorId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @ApiOperation(value = "Add Photo")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation photo"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed photo creation properties validation"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @PostMapping("/photos")
    public ResponseEntity<Photo> createPhoto(@Valid @RequestBody PhotoCreateRequest photoCreateRequest,
                                             @PathVariable("authorId") Long authorId) {
        this.validateAuthor(authorId);
        Photo photo = conversionService.convert(photoCreateRequest, Photo.class);

        if (photo == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        photo.setAuthor(userService.findUserById(authorId));
        return new ResponseEntity<>(photoService.save(photo), HttpStatus.CREATED);
    }


    @ApiOperation(value = "Find all Author Photos ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("/photos")
    public ResponseEntity<List<Photo>> findAuthorAllPhotos(@PathVariable("authorId") Long userId) {
        this.validateAuthor(userId);

        final List<Photo> photos = photoService.findPhotosByAuthorId(userId);

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Search any Photo by his id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @GetMapping("/photos/{photoId}")
    public ResponseEntity<Photo> findOnePhoto(@PathVariable("authorId") Long userId,
                                              @PathVariable("photoId") Long photoId) {
        this.validateAuthor(userId);

        return new ResponseEntity<>(photoService.findPhotoById(photoId), HttpStatus.OK);
    }


    @ApiOperation(value = "Search any Photo Licenses by his id ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading Licenses"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @GetMapping("/photos/{photoId}/licenses")
    public ResponseEntity<List<License>> findPhotoLicenses(@PathVariable("authorId") Long userId,
                                                           @PathVariable("photoId") Long photoId) {
        this.validateAuthor(userId);

        final List<License> licenses = licenseService.findLicensesByPhotoId(photoId);

        return licenses != null && !licenses.isEmpty()
                ? new ResponseEntity<>(licenses, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Add Licenses to Photo")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful created licenses"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @Transactional
    @PostMapping("/photos/{photoId}/licenses")
    public ResponseEntity<License> addLicense(@PathVariable("authorId") Long authorId,
                                              @PathVariable("photoId") Long photoId,
                                              @RequestParam LicenseType licenseName) {
        this.validateAuthor(authorId);
        this.validatePhoto(photoId, authorId);

        List<License> photoLicenses = licenseService.findLicensesByPhotoId(photoId);
        List<LicenseType> typeLicenses = photoLicenses.stream()
                .map(License::getLicenseType)
                .collect(Collectors.toList());
        if (typeLicenses.contains(licenseName)) {
            throw new ReAddException("This license has already been added to the photo with id = " + photoId);
        }

        LicenseCreateRequest request = new LicenseCreateRequest();
        request.setLicenseType(licenseName);
        request.setPhotoId(photoId);
        License license = conversionService.convert(request, License.class);

        return license != null
                ? new ResponseEntity<>(licenseService.save(license), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }


    @ApiOperation(value = "Delete Licenses")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful delete License"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "licenseId", value = "License id", example = "0", required = true,
                    dataType = "long", paramType = "path")
    })
    @Transactional
    @DeleteMapping("/photos/{photoId}/licenses/{licenseId}")
    public ResponseEntity<Void> deleteLicense(@PathVariable("authorId") Long authorId,
                                              @PathVariable("photoId") Long photoId,
                                              @PathVariable("licenseId") Long licenseId) {
        this.validateAuthor(authorId);
        this.validatePhoto(photoId, authorId);
        this.validateLicense(licenseId, photoId);

        licenseService.delete(licenseId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "Find Keywords to Photo")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find Keywords"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @GetMapping("/photos/{photoId}/keywords")
    public ResponseEntity<List<Keyword>> findKeywordsPhoto(@PathVariable("authorId") Long authorId,
                                                           @PathVariable("photoId") Long photoId) {
        this.validateAuthor(authorId);
        final List<Keyword> keywords = keywordService.findKeywordsByPhotoId(photoId);

        return keywords != null && !keywords.isEmpty()
                ? new ResponseEntity<>(keywords, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Add Keyword to Photo")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful add Keyword"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @Transactional
    @PutMapping("/photos/{photoId}/keywords")
    public ResponseEntity<Keyword> addKeywordToPhoto(@PathVariable("authorId") Long authorId,
                                                     @PathVariable("photoId") Long photoId,
                                                     @RequestParam String keywordName) {
        this.validateAuthor(authorId);
        this.validatePhoto(photoId, authorId);

        // we are looking for whether such a keyword exists in the database
        // if not, we create a new keyword
        Keyword keyword;
        Optional<Keyword> optional = keywordService.findKeywordByName(keywordName);
        if (optional.isPresent()) {
            keyword = optional.get();
        } else {
            KeywordCreateRequest request = new KeywordCreateRequest();
            request.setName(keywordName);
            keyword = conversionService.convert(request, Keyword.class);
            keyword = keywordService.save(keyword);
        }

        List<Keyword> listKeywords = keywordService.findKeywordsByPhotoId(photoId);
        List<String> nameKeywords = listKeywords.stream()
                .map(Keyword::getName)
                .collect(Collectors.toList());

        // check if this photo contains such a keyword.
        // if not, add this keyword to the photo
        if (nameKeywords.contains(keyword.getName().toLowerCase())) {
            throw new ReAddException("This photo contains this keyword");
        }

        photoService.addKeywordPhoto(photoId, keyword.getId());
        return new ResponseEntity<>(keywordService.findKeywordById(keyword.getId()), HttpStatus.OK);
    }


    @ApiOperation(value = "Delete Keyword")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful delete Keyword"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "keywordId", value = "Keyword id", example = "0", required = true,
                    dataType = "long", paramType = "path")
    })
    @DeleteMapping("/photos/{photoId}/keywords/{keywordId}")
    public ResponseEntity<Void> deleteKeyword(@PathVariable("authorId") Long authorId,
                                              @PathVariable("photoId") Long photoId,
                                              @PathVariable("keywordId") Long keywordId) {
        this.validateAuthor(authorId);
        this.validatePhoto(photoId, authorId);
        this.validateKeyword(keywordId, photoId);

        keywordService.delete(keywordId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "Find Category to Photo")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find Category"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @GetMapping("/photos/{photoId}/category")
    public ResponseEntity<Category> findPhotoCategory(@PathVariable("authorId") Long authorId,
                                                      @PathVariable("photoId") Long photoId) {
        this.validateAuthor(authorId);
        Photo photo = photoService.findPhotoById(photoId);
        Category category = categoryService.findCategoryByPhoto(photo);

        return category != null
                ? new ResponseEntity<>(category, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Add Category to Photo")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful add Category"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @Transactional
    @PutMapping("/photos/{photoId}/category")
    public ResponseEntity<Category> addCategoryToPhoto(@PathVariable("authorId") Long authorId,
                                                       @PathVariable("photoId") Long photoId,
                                                       @RequestParam CategoryName categoryName) {
        this.validateAuthor(authorId);
        this.validatePhoto(photoId, authorId);

        final Photo photo = photoService.findPhotoById(photoId);

        CategoryCreateRequest request = new CategoryCreateRequest();
        request.setName(categoryName);
        request.setPhotoId(photo.getId());
        Category category = conversionService.convert(request, Category.class);

        return category != null
                ? new ResponseEntity<>(categoryService.save(category), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete Category")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful delete Category"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorId", value = "Author id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "keywordId", value = "Keyword id", example = "0", required = true,
                    dataType = "long", paramType = "path")
    })
    @DeleteMapping("/photos/{photoId}/category/{categoryId}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("authorId") Long authorId,
                                               @PathVariable("photoId") Long photoId,
                                               @PathVariable("categoryId") Long categoryId) {
        this.validateAuthor(authorId);
        this.validatePhoto(photoId, authorId);
        this.validateCategory(categoryId, photoId);

        categoryService.delete(categoryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    private void validateAuthor(Long id) {
        if (!roleService.isAuthor(id)) {
            throw new ValidateException("This user is not the Author ");
        }

        if (userService.isDeleted(id)) {
            throw new ValidateException("This user is DELETED ");
        }

        if (!userService.isActive(id)) {
            throw new ValidateException("This user is BANNED");
        }
    }

    private void validatePhoto(Long photoId, Long authorId) {
        Photo photo = photoService.findPhotoById(photoId);

        if (!photo.getAuthor().getId().equals(authorId)) {
            throw new ValidateException("This photo does not belong to the author");
        }

        boolean isDeleted = photo.getIsDeleted();
        if (isDeleted) {
            throw new ValidateException("This photo is DELETED");
        }
    }

    private void validateLicense(Long licenseId, Long photoId) {
        License license = licenseService.findLicenseById(licenseId);

        if (!license.getPhoto().getId().equals(photoId)) {
            throw new ValidateException("This license does not belong to the photo");
        }

        boolean isDeleted = license.getIsDeleted();
        if (isDeleted) {
            throw new ValidateException("This license is DELETED ");
        }
    }

    private void validateCategory(Long categoryId, Long photoId) {
        Category category = categoryService.findCategoryById(categoryId);

        if (!category.getPhoto().getId().equals(photoId)) {
            throw new ValidateException("This category does not belong to this photo");
        }
    }

    private void validateKeyword(Long keywordId, Long photoId) {
        keywordService.findKeywordById(keywordId);
        linkKeywordPhotoService.findByKeywordAndPhoto(keywordId, photoId);
    }
}
