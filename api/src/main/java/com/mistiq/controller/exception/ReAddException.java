package com.mistiq.controller.exception;

public class ReAddException extends RuntimeException {

    public ReAddException() {
        super();
    }

    public ReAddException(String message) {
        super(message);
    }
}
