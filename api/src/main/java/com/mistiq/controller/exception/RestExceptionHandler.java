package com.mistiq.controller.exception;

import com.mistiq.controller.responces.ApiError;
import com.mistiq.exception.MyEntityException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger log = Logger.getLogger(RestExceptionHandler.class);

    @ExceptionHandler({MyEntityException.class, EntityNotFoundException.class})
    protected ResponseEntity<Object> handleEntityNotFoundEx(MyEntityException ex, WebRequest request) {
        ApiError apiError = new ApiError("MyEntity Not Found Exception", ex.getMessage());
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ValidateException.class})
    protected ResponseEntity<Object> handleEntityInvalidEx(ValidateException ex) {
        ApiError apiError = new ApiError("MyEntity Not Valid Exception", ex.getMessage());
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity<>(apiError, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({ReAddException.class})
    protected ResponseEntity<Object> handleReAddEx(ReAddException ex, WebRequest request) {
        ApiError apiError = new ApiError("Trying to add an existing parameter", ex.getMessage());
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    // unreadable - for example, invalid JSON.
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        ApiError apiError = new ApiError("Malformed JSON Request", ex.getMessage());
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity(apiError, status);
    }


    // invalid Entity. valid JSON, but @Valid condition is not met
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        ApiError apiError = new ApiError("Method Argument Not Valid", ex.getMessage(), errors);
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity<>(apiError, status);
    }


    // if no handler was found for this request
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
                                                                   HttpHeaders headers,
                                                                   HttpStatus status,
                                                                   WebRequest request) {
        return new ResponseEntity<Object>(new ApiError("No Handler Found", ex.getMessage()), status);
    }


    // the type of the passed argument is invalid
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        ApiError apiError = new ApiError();
        apiError.setErrorMessage(String.format("The parameter '%s' of value '%s' could not be converted to type '%s'",
                ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName()));
        apiError.setDebugMessage(ex.getMessage());
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity<>(apiError, BAD_REQUEST);
    }


    // default handler
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        ApiError apiError = new ApiError("Internal Exception", ex.getMessage());
        log.error(ex.getMessage(), ex);
        log.info(ex.getMessage(), ex);
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}