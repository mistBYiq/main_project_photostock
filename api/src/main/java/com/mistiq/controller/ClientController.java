package com.mistiq.controller;

import com.mistiq.controller.exception.ReAddException;
import com.mistiq.controller.exception.ValidateException;
import com.mistiq.controller.request.change.ClientChangeRequest;
import com.mistiq.controller.request.create.OrderCreateRequest;
import com.mistiq.domain.CategoryName;
import com.mistiq.domain.LicenseType;
import com.mistiq.domain.OrderStatus;
import com.mistiq.domain.RoleName;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Order;
import com.mistiq.domain.entity.Photo;
import com.mistiq.domain.entity.Role;
import com.mistiq.domain.entity.User;
import com.mistiq.service.CategoryService;
import com.mistiq.service.KeywordService;
import com.mistiq.service.LicenseService;
import com.mistiq.service.OrderService;
import com.mistiq.service.PhotoService;
import com.mistiq.service.RoleService;
import com.mistiq.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    public final UserService userService;

    public final RoleService roleService;

    public final CategoryService categoryService;

    public final KeywordService keywordService;

    public final PhotoService photoService;

    public final LicenseService licenseService;

    public final OrderService orderService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Change role to author")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated role"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clientId", value = "User id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @PatchMapping("/{clientId}/role")
    public ResponseEntity<Role> changeRoleToAuthor(@PathVariable("clientId") Long userId) {
        userService.findUserById(userId);
        Role role = roleService.findRoleByUserId(userId);

        if (roleService.isAuthor(userId)) {
            throw new ReAddException("This user has the role of Author ");
        }

        role.setName(RoleName.ROLE_AUTHOR);
        return new ResponseEntity<>(roleService.save(role), HttpStatus.OK);
    }


    @ApiOperation(value = "Update Client")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful updated client"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @Transactional(propagation = Propagation.REQUIRED)
    @PutMapping("/update")
    public ResponseEntity<User> updateUser(@Valid @RequestBody ClientChangeRequest request) {
        this.validateClient(request.getId());
        User user = conversionService.convert(request, User.class);

        return user != null
                ? new ResponseEntity<>(userService.save(user), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find all photos for Client. Search for all photos with parameters: " +
            "is_deleted: false and is_new: false")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @GetMapping("/find-photos")
    public ResponseEntity<List<Photo>> showAllPhotos() {
        final List<Photo> photos = photoService.findAllPhotosForClient();

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find photos by License for Client")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @GetMapping("/find-photos/by-license")
    public ResponseEntity<List<Photo>> findPhotosByLicense(@RequestParam("license") LicenseType licenseType) {
        final List<Photo> photos = photoService.findPhotosByLicenseTypeForClient(licenseType);

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find photos by Category for Client")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @GetMapping("/find-photos/by-category")
    public ResponseEntity<List<Photo>> findPhotosByCategory(@RequestParam("category") CategoryName categoryName) {
        final List<Photo> photos = photoService.findPhotosByCategoryNameForClient(categoryName);

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find photos by Keyword for Client")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "keyword", value = "Keyword name", example = "cool", required = true, dataType = "string",
            paramType = "query")
    @GetMapping("/find-photos/by-keyword")
    public ResponseEntity<List<Photo>> findPhotosByKeyword(@RequestParam("keyword") String keywordName) {
        final List<Photo> photos = photoService.findPhotosByKeywordNameForClient(keywordName);

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find photos by Name for Client")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "Photo name", example = "ph", required = true, dataType = "string",
                    paramType = "query")
    })
    @GetMapping("/find-photos/by-name")
    public ResponseEntity<List<Photo>> findPhotosByName(@RequestParam("name") String name) {
        final List<Photo> photos = photoService.findPhotosByNameForClient(name);

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find Licenses by Photo id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find licenses"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @GetMapping("/photos/{photoId}/licenses")
    public ResponseEntity<List<License>> findPhotoLicenses(@PathVariable("photoId") Long photoId) {
        final List<License> licenses = licenseService.findLicensesByPhotoId(photoId);

        return licenses != null && !licenses.isEmpty()
                ? new ResponseEntity<>(licenses, HttpStatus.OK)
                : new ResponseEntity<>(licenses, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Create Order")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful create order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @PostMapping("/{clientId}/orders")
    public ResponseEntity<Order> createOrder(@Valid @RequestBody OrderCreateRequest request,
                                             @PathVariable("clientId") Long clientId) {
        this.validateOrder(clientId, request.getLicenseId());

        Order order = conversionService.convert(request, Order.class);

        return order != null
                ? new ResponseEntity<>(orderService.save(order), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete Client Order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find order"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "Order id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @PutMapping("/{clientId}/orders/{orderId}")
    public ResponseEntity<Void> deleteOrder(@PathVariable("orderId") Long orderId,
                                            @PathVariable("clientId") Long clientId) {
        this.validateClient(clientId);
        Order order = orderService.findOrderById(orderId);

        if (order != null) {
            order.setIsDeleted(true);
            orderService.save(order);

            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
    }


    @ApiOperation(value = "Payment Order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successfully paid for the order"),
            @ApiResponse(code = 304, message = "Not Modified"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "Order id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @Transactional(propagation = Propagation.REQUIRED)
    @PutMapping("/{clientId}/orders/{orderId}/payment")
    public ResponseEntity<Order> paymentOrder(@PathVariable("orderId") Long orderId,
                                              @PathVariable("clientId") Long clientId) {
        this.validateClient(clientId);

        Order order = orderService.findOrderById(orderId);
        Integer orderPrice = order.getTotalPrice();

        User user = userService.findUserById(clientId);
        Integer userBalance = user.getBalance();

        if (orderPrice >= userBalance) {
            throw new ValidateException("Insufficient funds. Top up your balance");
        }

        userService.transfer(clientId, order.getAuthor().getId(), orderPrice);
        order.setOrderStatus(OrderStatus.APPROVED);

        return new ResponseEntity<>(orderService.save(order), HttpStatus.OK);
    }


    @ApiOperation(value = "Find approved orders by user id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find orders"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @GetMapping("/{clientId}/orders/approved")
    public ResponseEntity<List<Order>> showMyApprovedOrders(@PathVariable("clientId") Long clientId) {
        this.validateClient(clientId);
        final List<Order> orders = orderService.findApprovedOrdersByClientId(clientId);

        return orders != null && !orders.isEmpty()
                ? new ResponseEntity<>(orders, HttpStatus.OK)
                : new ResponseEntity<>(orders, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find not paid orders by user id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find orders"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @GetMapping("/{clientId}/orders/not_paid")
    public ResponseEntity<List<Order>> showMyNotPaidOrders(@PathVariable("clientId") Long clientId) {
        this.validateClient(clientId);
        final List<Order> orders = orderService.findNotPaidOrdersByClientId(clientId);

        return orders != null && !orders.isEmpty()
                ? new ResponseEntity<>(orders, HttpStatus.OK)
                : new ResponseEntity<>(orders, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find Order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find order"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string"),
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "orderId", value = "Order id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @GetMapping("/{clientId}/orders/{orderId}")
    public ResponseEntity<Order> showOrderById(@PathVariable("clientId") Long clientId,
                                               @PathVariable("orderId") Long orderId) {
        this.validateClient(clientId);
        final Order order = orderService.findOrderByUserIdAndId(clientId, orderId);

        return order != null
                ? new ResponseEntity<>(order, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find Client approved licenses")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find licenses"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string"),
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @GetMapping("/{clientId}/licenses")
    public ResponseEntity<List<License>> showMyLicenses(@PathVariable("clientId") Long clientId) {
        this.validateClient(clientId);
        List<Order> orders = orderService.findApprovedOrdersByClientId(clientId);

        List<License> licenses = orders.stream()
                .map(Order::getLicense)
                .collect(Collectors.toList());

        return !licenses.isEmpty()
                ? new ResponseEntity<>(licenses, HttpStatus.OK)
                : new ResponseEntity<>(licenses, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find Client approved photos")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find licenses"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string"),
            @ApiImplicitParam(name = "clientId", value = "Client id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @Secured({"ROLE_CLIENT", "ROLE_ADMIN"})
    @GetMapping("/{clientId}/photos")
    public ResponseEntity<List<Photo>> showMyPhotos(@PathVariable("clientId") Long clientId) {
        this.validateClient(clientId);
        List<Order> orders = orderService.findApprovedOrdersByClientId(clientId);

        List<Photo> photos = orders.stream()
                .map(Order::getPhoto)
                .collect(Collectors.toList());

        return !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    private void validateClient(Long id) {
        if (!roleService.isClient(id)) {
            throw new ValidateException("This user is not the client ");
        }

        if (userService.isDeleted(id)) {
            throw new ValidateException("This user is DELETED ");
        }

        if (!userService.isActive(id)) {
            throw new ValidateException("This user is BANNED");
        }
    }


    private void validateOrder(Long clientId, Long licenseId) {
        validateClient(clientId);

        licenseService.findLicenseById(licenseId);

        if (orderService.findOrderByUserIdAndLicenseId(clientId, licenseId)) {
            throw new ReAddException("This license with id = " + licenseId +
                    " has already been added to this client with id = " + clientId);
        }
    }
}
