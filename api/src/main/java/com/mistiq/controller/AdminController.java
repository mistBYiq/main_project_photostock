package com.mistiq.controller;

import com.mistiq.controller.request.create.UserCreateRequest;
import com.mistiq.domain.Gender;
import com.mistiq.domain.UserStatus;
import com.mistiq.domain.entity.Photo;
import com.mistiq.domain.entity.Role;
import com.mistiq.domain.entity.User;
import com.mistiq.repository.impl.PhotoRepository;
import com.mistiq.service.CategoryService;
import com.mistiq.service.KeywordService;
import com.mistiq.service.OrderService;
import com.mistiq.service.PhotoService;
import com.mistiq.service.RoleService;
import com.mistiq.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/admins")
@RequiredArgsConstructor
public class AdminController {

    public final UserService userService;

    public final RoleService roleService;

    public final CategoryService categoryService;

    public final KeywordService keywordService;

    public final PhotoService photoService;

    public final PhotoRepository photoRepository;

    public final OrderService orderService;

    public final ConversionService conversionService;

    public final CacheManager cacheManager;

    @ApiOperation(value = "Endpoint for creation USER", response = User.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation user"),
            @ApiResponse(code = 400, message = "Unprocessable Entity. Failed user creation properties validation"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string"),

            @ApiImplicitParam(name = "name", value = "Name ", example = "User", defaultValue = "user", dataType = "string", required = true, paramType = "query"),
            @ApiImplicitParam(name = "surname", value = "Surname", example = "Surname", defaultValue = "surname", dataType = "string", required = true, paramType = "query"),
            @ApiImplicitParam(name = "dateBirth", value = "dateBirth", example = "dd.mm.yyyy", defaultValue = "13.02.1989", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "gender", value = "Gender", example = "NOT_SELECTED", defaultValue = "NOT_SELECTED", dataTypeClass = Gender.class, paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "phone", example = "+59379992", defaultValue = "9379992", dataType = "string", paramType = "query"),

            @ApiImplicitParam(name = "email", value = "Email ", example = "User@email.com", defaultValue = "0", dataType = "string", required = true, paramType = "query"),
            @ApiImplicitParam(name = "password", value = "Password ", example = "password", defaultValue = "password", dataType = "string", required = true, paramType = "query"),
            @ApiImplicitParam(name = "login", value = "Login ", example = "Login", defaultValue = "login", dataType = "string", required = true, paramType = "query"),

            @ApiImplicitParam(name = "country", value = "country", example = "Belarus", defaultValue = "Belarus", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "city", value = "city", example = "Minsk", defaultValue = "Minsk", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "street", value = "street", example = "Makaenka", defaultValue = "Makaenka", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "postalCode", value = "postalCode", example = "2110001", defaultValue = "2110001", dataType = "string", paramType = "query"),

            @ApiImplicitParam(name = "isDeleted", value = "isDeleted", example = "false", defaultValue = "false", dataType = "boolean", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "status", example = "ACTIVE", defaultValue = "ACTIVE", dataTypeClass = UserStatus.class, paramType = "query"),
            @ApiImplicitParam(name = "balance", value = "Balance", example = "0", defaultValue = "0", dataType = "integer", paramType = "query")

    })
    @PostMapping("/users")
    public ResponseEntity<User> createUser(@ApiIgnore @Valid UserCreateRequest userCreateRequest) {
        User user = conversionService.convert(userCreateRequest, User.class);

        return user != null
                ? new ResponseEntity<>(userService.save(user), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find All users with role admin")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find users"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @GetMapping()
    public ResponseEntity<List<User>> findAllAdmins() {
        final List<User> users = userService.findAllUsersWithRoleAdmin();

        return users != null
                ? new ResponseEntity<>(users, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find All photos by name and description. Criteria")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find users"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
            //
            //
            //
    })
    @GetMapping("/photos/{name}/{description}")
    public ResponseEntity<List<Photo>> findByCriteria(@PathVariable("name") String name,
                                                      @PathVariable("description") String description) {
        try {
            final List<Photo> photos = photoService.findByNameAndDescriptionCriteria(name, description);
            return new ResponseEntity<>(photos, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @ApiOperation(value = "Find All users with status banned")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find users"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @GetMapping("/users/baned")
    public ResponseEntity<List<User>> findAllUsersWithStatusBanned() {
        final List<User> users = userService.findAllWithStatusBanned();

        return users != null && !users.isEmpty()
                ? new ResponseEntity<>(users, HttpStatus.OK)
                : new ResponseEntity<>(users, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Banned user by email")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful banned user"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "User email", example = "user@mail.com", required = true,
                    dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @PostMapping("users/ban")
    public ResponseEntity<Void> banUser(@RequestParam("email") String email) {
        userService.banByEmail(email);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "Unbanned user by email")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful unbanned user"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "User email", example = "user@mail.com", required = true,
                    dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @PostMapping("/users/unban")
    public ResponseEntity<Void> unbanUser(@RequestParam("email") String email) {
        userService.unbanByEmail(email);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "Find All Photos")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "page", value = "Page number", example = "0", defaultValue = "0",
                    dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "Items per page", example = "10", defaultValue = "3",
                    dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "Field to sort", example = "name", defaultValue = "name",
                    dataType = "string", paramType = "query")
    })
    @GetMapping("/photos")
    public ResponseEntity<Page<Photo>> findAllPhotosWithPagination(@ApiIgnore Pageable pageable) {
        Page<Photo> usersPage = photoRepository.findAll(pageable);
        return new ResponseEntity<>(usersPage, HttpStatus.OK);
    }


    @ApiOperation(value = "Find All photos with status New")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find photos"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, dataType = "string",
            paramType = "header")
    @GetMapping("/photos/new")
    public ResponseEntity<List<Photo>> allNewPhotos() {
        final List<Photo> photos = photoService.findPhotosByIsNewTrue();

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Photo check")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful photo check"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, dataType = "string",
                    paramType = "header"),
            @ApiImplicitParam(name = "photoId", value = "Photo id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @PostMapping("/photos/{photoId}/check")
    public ResponseEntity<Photo> checkedNewPhoto(@PathVariable("photoId") Long id) {
        Photo photo = photoService.checkedNewPhoto(id);

        return photo != null
                ? new ResponseEntity<>(photoService.save(photo), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }


    @ApiOperation(value = "Find user by login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful photo check"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, dataType = "string",
                    paramType = "header"),
            @ApiImplicitParam(name = "login", value = "User login", example = "login", required = true,
                    dataType = "string", paramType = "path")
    })
    @GetMapping("/users/{login}")
    public ResponseEntity<User> findUserByLogin(@PathVariable("login") String login) {
        final User user = userService.findUserByLogin(login);

        return user != null
                ? new ResponseEntity<>(user, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find User roles")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful find roles"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, dataType = "string",
                    paramType = "header"),
            @ApiImplicitParam(name = "userId", value = "User id", example = "0", required = true, dataType = "long",
                    paramType = "path")
    })
    @GetMapping("/users/{userId}/roles")
    public ResponseEntity<Role> findUserRole(@PathVariable("userId") Long id) {
        userService.findUserById(id);
        final Role role = roleService.findRoleByUserId(id);

        return role != null
                ? new ResponseEntity<>(role, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Test Cache")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading keywords"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @GetMapping("/caches")
    public ResponseEntity<Object> getCachesInfo() {
        return new ResponseEntity<>(String.join(",", cacheManager.getCacheNames()), HttpStatus.OK);
    }
}