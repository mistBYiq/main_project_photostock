package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.PhotoChangeRequest;
import com.mistiq.controller.request.create.PhotoCreateRequest;
import com.mistiq.domain.entity.Photo;
import com.mistiq.service.PhotoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/photos")
@RequiredArgsConstructor
public class PhotoRestController {

    public final PhotoService photoService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Creation Photo")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation photo"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed photo creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @PostMapping
    public ResponseEntity<Photo> createPhoto(@Valid @RequestBody PhotoCreateRequest photoCreateRequest) {
        Photo photo = conversionService.convert(photoCreateRequest, Photo.class);

        return photo != null
                ? new ResponseEntity<>(photoService.save(photo), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find all photos")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading photos"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @GetMapping
    public ResponseEntity<List<Photo>> findAllPhotos() {
        final List<Photo> photos = photoService.findAll();

        return photos != null && !photos.isEmpty()
                ? new ResponseEntity<>(photos, HttpStatus.OK)
                : new ResponseEntity<>(photos, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find photo by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading photo"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Photo id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("/{id}")
    public ResponseEntity<Photo> getPhoto(@PathVariable("id") Long id) {
        return new ResponseEntity<>(photoService.findPhotoById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Update photo by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful update photo"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Photo id", example = "0", required = true,
            dataType = "long", paramType = "path")
    @PutMapping("/{id}")
    public ResponseEntity<Photo> updatePhoto(@PathVariable("id") Long id,
                                             @Valid @RequestBody PhotoCreateRequest request) {
        Photo photo = photoService.findPhotoById(id);
        photo.setName(request.getName());
        photo.setDescription(request.getDescription());
        photo.setChanged(new Timestamp(new Date().getTime()));

        return new ResponseEntity<>(photoService.save(photo), HttpStatus.OK);
    }


    @ApiOperation(value = "Update photo")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful update photo"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed photo update properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @PutMapping
    public ResponseEntity<Photo> updatePhoto(@Valid @RequestBody PhotoChangeRequest photoChangeRequest) {
        Photo photo = conversionService.convert(photoChangeRequest, Photo.class);

        return photo != null
                ? new ResponseEntity<>(photoService.save(photo), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete photo by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleting photo"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Photo id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePhoto(@PathVariable("id") Long id) {
        photoService.deletePhoto(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}