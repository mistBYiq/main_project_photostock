package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.KeywordChangeRequest;
import com.mistiq.controller.request.create.KeywordCreateRequest;
import com.mistiq.domain.entity.Keyword;
import com.mistiq.service.KeywordService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/keywords")
@RequiredArgsConstructor
public class KeywordRestController {

    public final KeywordService keywordService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Endpoint for creation keyword (using @CachePut)")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation keyword"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed keyword creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping
    public ResponseEntity<Keyword> createKeyword(@Valid @RequestBody KeywordCreateRequest request) {
        Keyword keyword = conversionService.convert(request, Keyword.class);

        return keyword != null
                ? new ResponseEntity<>(keywordService.save(keyword), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find all keywords")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading keywords"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping
    public ResponseEntity<List<Keyword>> findAllKeyword() {
        final List<Keyword> keywords = keywordService.findAll();

        return keywords != null && !keywords.isEmpty()
                ? new ResponseEntity<>(keywords, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find keyword by id (with Cache)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading keywords"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Keyword id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("/{id}/caches")
    public ResponseEntity<Keyword> findKeywordWithCache(@PathVariable("id") Long id) {
        final Keyword keyword = keywordService.findKeywordByIdWithCache(id);

        return keyword != null
                ? new ResponseEntity<>(keyword, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find keyword by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading keyword"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Keyword id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("/{id}")
    public ResponseEntity<Keyword> getKeyword(@PathVariable("id") Long id) {
        final Keyword keyword = keywordService.findKeywordById(id);

        return keyword != null
                ? new ResponseEntity<>(keyword, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Update keyword by id (using @CachePut)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated keyword"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Keyword id", example = "0", required = true,
            dataType = "long", paramType = "path")
    @PutMapping("/{id}")
    public ResponseEntity<Keyword> updateKeyword(@PathVariable("id") Long id,
                                                 @Valid @RequestBody KeywordCreateRequest request) {
        Keyword keyword = keywordService.findKeywordById(id);
        keyword.setName(request.getName());
        keyword.setChanged(new Timestamp(new Date().getTime()));

        return new ResponseEntity<>(keywordService.save(keyword), HttpStatus.OK);
    }


    @ApiOperation(value = "Update keyword (using @CachePut)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated keyword"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed keyword creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping
    public ResponseEntity<Keyword> updateKeyword(@Valid @RequestBody KeywordChangeRequest request) {
        Keyword keyword = conversionService.convert(request, Keyword.class);

        return keyword != null
                ? new ResponseEntity<>(keywordService.save(keyword), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete keyword by id (using @CacheEvict)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleting keyword"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Keyword id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteKeyword(@PathVariable("id") Long id) {
        keywordService.deleteKeyword(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
