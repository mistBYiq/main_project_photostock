package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.LicenseChangeRequest;
import com.mistiq.controller.request.create.LicenseCreateRequest;
import com.mistiq.domain.entity.License;
import com.mistiq.service.LicenseService;
import com.mistiq.service.PhotoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/licenses")
@RequiredArgsConstructor
public class LicenseRestController {

    public final LicenseService licenseService;

    public final PhotoService photoService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Endpoint for creation license")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation license"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed license creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping
    public ResponseEntity<License> createLicense(@Valid @RequestBody LicenseCreateRequest licenseCreateRequest) {
        License license = conversionService.convert(licenseCreateRequest, License.class);

        return license != null
                ? new ResponseEntity<>(licenseService.save(license), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find all licenses")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading licenses"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping
    public ResponseEntity<List<License>> findAllLicense() {
        final List<License> licenses = licenseService.findAll();

        return licenses != null && !licenses.isEmpty()
                ? new ResponseEntity<>(licenses, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find license by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading license"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "License id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("{id}")
    public ResponseEntity<License> getLicense(@PathVariable("id") Long id) {
        return new ResponseEntity<>(licenseService.findLicenseById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Update license by id")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful creation license"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "License id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @PutMapping("/{id}")
    public ResponseEntity<License> updateLicense(@PathVariable("id") Long id,
                                                 @Valid @RequestBody LicenseCreateRequest request) {
        License license = licenseService.findLicenseById(id);
        license.setLicenseType(request.getLicenseType());
        license.setPrice(request.getLicenseType().getRepresentation());
        license.setPhoto(photoService.findPhotoById(request.getPhotoId()));
        license.setChanged(new Timestamp(new Date().getTime()));

        return new ResponseEntity<>(licenseService.save(license), HttpStatus.OK);
    }


    @ApiOperation(value = "Update license")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful creation license"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed license creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping
    public ResponseEntity<License> updateLicense(@Valid @RequestBody LicenseChangeRequest request) {
        License license = conversionService.convert(request, License.class);

        return license != null
                ? new ResponseEntity<>(licenseService.save(license), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete license by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleting license"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "License id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteLicense(@PathVariable("id") Long id) {
        licenseService.deleteLicense(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
