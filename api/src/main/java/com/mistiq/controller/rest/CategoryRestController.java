package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.CategoryChangeRequest;
import com.mistiq.controller.request.create.CategoryCreateRequest;
import com.mistiq.domain.entity.Category;
import com.mistiq.service.CategoryService;
import com.mistiq.service.PhotoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/categories")
@RequiredArgsConstructor
public class CategoryRestController {

    public final CategoryService categoryService;

    public final PhotoService photoService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Endpoint for creation category")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation category"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed category creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping
    public ResponseEntity<Category> createCategory(@Valid @RequestBody CategoryCreateRequest request) {
        Category category = conversionService.convert(request, Category.class);

        return category != null
                ? new ResponseEntity<>(categoryService.save(category), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find all categories")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading categories"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping
    public ResponseEntity<List<Category>> findAllCategories() {
        final List<Category> categories = categoryService.findAll();

        return categories != null && !categories.isEmpty()
                ? new ResponseEntity<>(categories, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find category by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading category"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Category id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("/{id}")
    public ResponseEntity<Category> getCategory(@PathVariable("id") Long id) {

        return new ResponseEntity<>(categoryService.findCategoryById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Update category by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated category"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Category id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @PutMapping("/{id}")
    public ResponseEntity<Category> updateCategory(@PathVariable("id") Long id,
                                                   @Valid @RequestBody CategoryCreateRequest request) {
        Category category = categoryService.findCategoryById(id);
        category.setName(request.getName());
        category.setPhoto(photoService.findPhotoById(request.getPhotoId()));
        category.setChanged(new Timestamp(new Date().getTime()));

        return new ResponseEntity<>(categoryService.save(category), HttpStatus.OK);
    }


    @ApiOperation(value = "Update category")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated category"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed category creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping
    public ResponseEntity<Category> updateCategory(@Valid @RequestBody CategoryChangeRequest request) {
        Category category = conversionService.convert(request, Category.class);

        return category != null
                ? new ResponseEntity<>(categoryService.save(category), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete category by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleting category"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Error. Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Category id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("id") Long id) {
        categoryService.deleteCategory(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
