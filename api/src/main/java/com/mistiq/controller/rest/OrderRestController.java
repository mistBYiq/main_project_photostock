package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.OrderChangeRequest;
import com.mistiq.controller.request.create.OrderCreateRequest;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Order;
import com.mistiq.service.LicenseService;
import com.mistiq.service.OrderService;
import com.mistiq.service.PhotoService;
import com.mistiq.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/orders")
@RequiredArgsConstructor
public class OrderRestController {

    public final OrderService orderService;

    public final UserService userService;

    public final LicenseService licenseService;

    public final PhotoService photoService;

    public final ConversionService conversionService;


    @ApiOperation(value = "Endpoint for creation order")
    @Secured("ROLE_ADMIN")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed order creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping
    public ResponseEntity<Order> createOrder(@Valid @RequestBody OrderCreateRequest orderCreateRequest) {
        Order order = conversionService.convert(orderCreateRequest, Order.class);

        return order != null
                ? new ResponseEntity<>(orderService.save(order), HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Find all orders")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading orders"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping
    public ResponseEntity<List<Order>> findAllOrder() {
        final List<Order> orders = orderService.findAll();

        return orders != null && !orders.isEmpty()
                ? new ResponseEntity<>(orders, HttpStatus.OK)
                : new ResponseEntity<>(orders, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Order id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @GetMapping("/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable("id") Long id) {
        return new ResponseEntity<>(orderService.findOrderById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Update order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Order id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @PutMapping("/{id}")
    public ResponseEntity<Order> updateOrder(@PathVariable("id") Long id,
                                             @Valid @RequestBody OrderCreateRequest request) {
        Order order = orderService.findOrderById(id);

        order.setUser(userService.findUserById(request.getUserId()));
        License license = licenseService.findLicenseById(request.getLicenseId());
        order.setLicense(license);
        order.setTotalPrice(license.getPrice());
        order.setPhoto(photoService.findPhotoById(license.getPhoto().getId()));
        order.setAuthor(userService.findUserById(license.getPhoto().getAuthor().getId()));
        order.setChanged(new Timestamp(new Date().getTime()));

        return new ResponseEntity<>(orderService.save(order), HttpStatus.OK);
    }


    @ApiOperation(value = "Update order")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful updated order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable entity. Failed order creation properties validation"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping
    public ResponseEntity<Order> updateOrder(@Valid @RequestBody OrderChangeRequest orderChangeRequest) {
        Order order = conversionService.convert(orderChangeRequest, Order.class);

        return order != null
                ? new ResponseEntity<>(orderService.save(order), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleted order"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @ApiImplicitParam(name = "id", value = "Order id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable("id") Long id) {
        orderService.deleteOrder(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
