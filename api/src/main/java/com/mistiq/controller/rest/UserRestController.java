package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.UserChangeRequest;
import com.mistiq.controller.request.create.UserCreateRequest;
import com.mistiq.domain.Address;
import com.mistiq.domain.Credentials;
import com.mistiq.domain.entity.User;
import com.mistiq.service.UserService;
import com.mistiq.util.MyConverters;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/users")
@RequiredArgsConstructor
public class UserRestController {

    public final UserService userService;

    public final ConversionService conversionService;

    public final MyConverters converterService;


    @ApiOperation(value = "Endpoint for creation user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation user"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @PostMapping
    public ResponseEntity<User> createUser(@Valid @RequestBody UserCreateRequest userCreateRequest) {
        User user = conversionService.convert(userCreateRequest, User.class);
        return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
    }


    @ApiOperation(value = "Find all users")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading users"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @GetMapping
    public ResponseEntity<List<User>> findAllUsers() {
        final List<User> users = userService.findAll();

        return users != null && !users.isEmpty()
                ? new ResponseEntity<>(users, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading user"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User id", example = "0", required = true,
                    dataType = "long", paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
        return new ResponseEntity<>(userService.findUserById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Update user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful update user"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User id", example = "0", required = true, dataType = "long",
                    paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id,
                                           @Valid @RequestBody UserCreateRequest userCreateRequest) {
        User user = userService.findUserById(id);
        user.setName(userCreateRequest.getName());
        user.setSurname(userCreateRequest.getSurname());
        user.setDateBirth(converterService.convertStringToDate(userCreateRequest.getDateBirth()));
        user.setGender(userCreateRequest.getGender());
        user.setPhone(userCreateRequest.getPhone());
        user.setEmail(userCreateRequest.getEmail());
        user.setCredential(new Credentials(userCreateRequest.getPassword(),
                userCreateRequest.getLogin()));
        user.setAddress(new Address(userCreateRequest.getCountry(),
                userCreateRequest.getCity(),
                userCreateRequest.getPostalCode(),
                userCreateRequest.getStreet()));
        user.setChanged(new Timestamp(new Date().getTime()));
        user.setBalance(userCreateRequest.getBalance());

        return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
    }


    @ApiOperation(value = "Update user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful update user"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @Transactional(propagation = Propagation.REQUIRED)
    @PutMapping
    public ResponseEntity<User> updateUser(@Valid @RequestBody UserChangeRequest userChangeRequest) {
        User user = conversionService.convert(userChangeRequest, User.class);

        return user != null
                ? new ResponseEntity<>(userService.save(user), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @ApiOperation(value = "Delete user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleting user"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User id", example = "0", required = true,
                    dataType = "long", paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
                    dataType = "string")
    })
    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "Search Users by containing name")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK. Successful deleting user"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "X-Auth-Token", defaultValue = "token", required = true, paramType = "header",
            dataType = "string")
    @GetMapping("/search")
    public ResponseEntity<List<User>> searchUsersByNameContaining(@RequestParam("search") String name) {
        List<User> users = userService.search(name);

        return users != null && !users.isEmpty()
                ? new ResponseEntity<>(users, HttpStatus.OK)
                : new ResponseEntity<>(users, HttpStatus.NOT_FOUND);
    }
}
