package com.mistiq.controller.rest;

import com.mistiq.controller.request.change.RoleChangeRequest;
import com.mistiq.controller.request.create.RoleCreateRequest;
import com.mistiq.domain.RoleName;
import com.mistiq.domain.entity.Role;
import com.mistiq.domain.entity.User;
import com.mistiq.repository.impl.RoleRepository;
import com.mistiq.service.RoleService;
import com.mistiq.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rest/roles")
@RequiredArgsConstructor
public class RoleRestController {

    public final RoleService roleService;

    public final UserService userService;

    public final RoleRepository roleRepository;

    @ApiOperation(value = "Endpoint creation role")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created. Successful creation role"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed role creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @PostMapping
    public ResponseEntity<Role> createRole(@Valid @RequestBody RoleCreateRequest request) {
        User user = userService.findUserById(request.getUserId());

        Role role = new Role();
        role.setName(request.getName());
        role.setUser(user);

        return new ResponseEntity<>(roleService.save(role), HttpStatus.CREATED);
    }


    @ApiOperation(value = "Find all roles")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading roles"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @GetMapping
    public ResponseEntity<List<Role>> findAllRoles() {
        final List<Role> roles = roleRepository.findAll();

        return !roles.isEmpty()
                ? new ResponseEntity<>(roles, HttpStatus.OK)
                : new ResponseEntity<>(roles, HttpStatus.NOT_FOUND);
    }


    @ApiOperation(value = "Find role by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful loading role"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Role id", example = "0", required = true,
            dataType = "long", paramType = "path")
    @GetMapping("/{id}")
    public ResponseEntity<Role> getRole(@PathVariable("id") Long id) {
        return new ResponseEntity<>(roleService.findRoleById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Update role by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful creation role"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Role id", example = "0", required = true,
            dataType = "long", paramType = "path")
    @PutMapping("/{id}")
    public ResponseEntity<Role> updateRole(@PathVariable("id") Long id,
                                           @Valid @RequestBody RoleCreateRequest request) {
        User user = userService.findUserById(request.getUserId());

        Role role = roleService.findRoleById(id);
        role.setName(request.getName());
        role.setUser(user);

        return new ResponseEntity<>(roleService.save(role), HttpStatus.OK);
    }


    @ApiOperation(value = "Change role name")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Ok. Successful creation role"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity. Failed role creation properties validation"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @PutMapping
    public ResponseEntity<Role> changeRoleName(@Valid @RequestBody RoleChangeRequest roleChangeRequest,
                                               @RequestParam RoleName roleName) {
        Role role = roleService.findRoleById(roleChangeRequest.getId());
        role.setName(roleName);

        return new ResponseEntity<>(roleService.save(role), HttpStatus.OK);
    }


    @ApiOperation(value = "Delete role by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok. Successful deleting role"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ApiImplicitParam(name = "id", value = "Role id", example = "0", required = true, dataType = "long",
            paramType = "path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRole(@PathVariable("id") Long id) {
        roleService.deleteRole(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}