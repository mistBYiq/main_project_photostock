package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.PhotoCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class PhotoChangeRequest extends PhotoCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Photo id")
    private Long id;
}
