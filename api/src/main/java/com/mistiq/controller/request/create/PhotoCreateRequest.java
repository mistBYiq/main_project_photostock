package com.mistiq.controller.request.create;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@ApiModel(description = "Photo creation model")
public class PhotoCreateRequest {

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 50)
    @ApiModelProperty(required = true, dataType = "string", notes = "photo name")
    private String name;

    @Size(min = 2, max = 200)
    @ApiModelProperty(dataType = "string", notes = "description photo")
    private String description;
}