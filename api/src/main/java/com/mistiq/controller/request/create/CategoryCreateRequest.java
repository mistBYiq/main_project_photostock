package com.mistiq.controller.request.create;

import com.mistiq.domain.CategoryName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "Category creation model")
public class CategoryCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "string", notes = "Category name")
    private CategoryName name;

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Photo id")
    private Long photoId;

}
