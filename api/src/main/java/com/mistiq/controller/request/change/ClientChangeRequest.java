package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.ClientCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class ClientChangeRequest extends ClientCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Client id")
    private Long id;
}
