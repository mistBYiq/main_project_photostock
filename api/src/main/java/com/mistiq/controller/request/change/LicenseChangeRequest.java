package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.LicenseCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class LicenseChangeRequest extends LicenseCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "License id")
    private Long id;
}
