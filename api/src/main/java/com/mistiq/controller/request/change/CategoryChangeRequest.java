package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.CategoryCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryChangeRequest extends CategoryCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Category id")
    private Long id;
}
