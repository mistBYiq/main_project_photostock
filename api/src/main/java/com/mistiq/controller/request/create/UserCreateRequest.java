package com.mistiq.controller.request.create;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.mistiq.domain.Gender;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "User creation model")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UserCreateRequest {

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 50)
    @ApiModelProperty(required = true, example = "Ivan", dataType = "string", notes = "user first name")
    private String name;

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 50)
    @ApiModelProperty(required = true, example = "Ivanov", dataType = "string", notes = "user last name")
    private String surname;

    @Size(min = 8, max = 10)
    @Pattern(regexp = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$")
    @ApiModelProperty(dataType = "string", example = "01.01.1989", notes = "user birthdate ")
    private String dateBirth;

    @ApiModelProperty(dataType = "string", example = "NOT_SELECTED", notes = "user gender")
    private Gender gender;

    @Size(min = 7, max = 150)
    @ApiModelProperty(dataType = "string", example = "+375291001010", notes = "phone number")
    private String phone;

    @NotNull
    @NotEmpty
    @Size(min = 7, max = 150)
    @ApiModelProperty(required = true, example = "uniqUserMail@xmail.com", dataType = "string", notes = "uniq email")
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 8, max = 100)
    @ApiModelProperty(required = true, example = "password", dataType = "string", notes = "password")
    private String password;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 50)
    @ApiModelProperty(required = true, example = "uniqUserLogin", dataType = "string", notes = "uniq login")
    private String login;

    @Size(min = 4, max = 100)
    @ApiModelProperty(required = true, example = "Republic of Belarus", dataType = "string", notes = "country name")
    private String country;

    @Size(min = 2, max = 100)
    @ApiModelProperty(dataType = "string", example = "Minsk", notes = "city name")
    private String city;

    @Size(min = 2, max = 100)
    @ApiModelProperty(dataType = "string", example = "Makaenka", notes = "street name ")
    private String street;

    @Size(min = 3, max = 30)
    @ApiModelProperty(dataType = "string", example = "2110001", notes = "postal code")
    private String postalCode;

    @ApiModelProperty(required = true, dataType = "integer", notes = "balance")
    private Integer balance;
}