package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.RoleCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class RoleChangeRequest extends RoleCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Role id")
    private Long id;
}
