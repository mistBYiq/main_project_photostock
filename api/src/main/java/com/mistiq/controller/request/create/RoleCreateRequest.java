package com.mistiq.controller.request.create;

import com.mistiq.domain.RoleName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "Role creation model")
public class RoleCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Role name")
    private RoleName name;

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "User id")
    private Long userId;
}