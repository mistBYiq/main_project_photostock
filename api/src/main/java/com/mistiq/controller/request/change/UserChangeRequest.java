package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.UserCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserChangeRequest extends UserCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "User id")
    private Long id;
}
