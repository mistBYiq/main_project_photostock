package com.mistiq.controller.request.create;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "Order creation model")
public class OrderCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "User id")
    private Long userId;

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "License id")
    private Long licenseId;
}
