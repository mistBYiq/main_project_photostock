package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.OrderCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class OrderChangeRequest extends OrderCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Order id")
    private Long id;
}
