package com.mistiq.controller.request.change;

import com.mistiq.controller.request.create.KeywordCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class KeywordChangeRequest extends KeywordCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Keyword id")
    private Long id;
}
