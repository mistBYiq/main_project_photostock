package com.mistiq.controller.request.create;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@ApiModel(description = "Keyword creation model")
public class KeywordCreateRequest {

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 30)
    @ApiModelProperty(required = true, dataType = "string", notes = "keyword", example = "keyword")
    private String name;
}
