package com.mistiq.controller.request.create;

import com.mistiq.domain.Gender;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "test. Client creation model")
public class ClientCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "string", notes = "Client name", example = "Fedor")
    private String name;

    @NotNull
    @ApiModelProperty(required = true, dataType = "string", notes = "Client surname", example = "Smith")
    private String surname;

    @ApiModelProperty(dataType = "string", notes = "Date Birth", example = "01.01.1989")
    private String dateBirth;

    @ApiModelProperty(dataType = "string", notes = "Gender")
    private Gender gender;

    @NotNull
    @ApiModelProperty(required = true, dataType = "string", notes = "Unique email", example = "uniqMail@mail.com")
    private String email;

    @ApiModelProperty(dataType = "string", notes = "Phone number", example = "+375291001010")
    private String phone;

    @ApiModelProperty(dataType = "string", notes = "country name", example = "Republic of Belarus")
    private String country;

    @ApiModelProperty(dataType = "string", notes = "City name", example = "Minsk")
    private String city;

    @ApiModelProperty(dataType = "string", notes = "Street name", example = "Krasnaay")
    private String street;

    @ApiModelProperty(dataType = "string", notes = "postal code", example = "2110001")
    private String postalCode;

    @ApiModelProperty(dataType = "integer", notes = "balance")
    private Integer balance;

}
