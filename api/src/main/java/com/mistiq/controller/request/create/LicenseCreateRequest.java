package com.mistiq.controller.request.create;

import com.mistiq.domain.LicenseType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "License creation model")
public class LicenseCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "string", notes = "License type")
    private LicenseType licenseType;

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Photo id")
    private Long photoId;
}
