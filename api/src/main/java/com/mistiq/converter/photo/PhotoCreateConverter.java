package com.mistiq.converter.photo;


import com.mistiq.controller.request.create.PhotoCreateRequest;
import com.mistiq.domain.entity.Photo;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class PhotoCreateConverter extends PhotoConverter<PhotoCreateRequest, Photo> {

    @Override
    public Photo convert(PhotoCreateRequest request) {
        Photo photo = new Photo();

        photo.setCreated(new Timestamp(new Date().getTime()));
        photo.setChanged(new Timestamp(new Date().getTime()));
        photo.setIsDeleted(false);
        photo.setIsNew(true);

        return doConvert(photo, request);
    }
}
