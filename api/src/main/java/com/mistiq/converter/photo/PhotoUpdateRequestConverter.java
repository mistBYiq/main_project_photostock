package com.mistiq.converter.photo;

import com.mistiq.controller.request.change.PhotoChangeRequest;
import com.mistiq.domain.entity.Photo;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.PhotoRepository;
import org.springframework.stereotype.Component;

@Component
public class PhotoUpdateRequestConverter extends PhotoConverter<PhotoChangeRequest, Photo> {

    private final PhotoRepository photoRepository;

    public PhotoUpdateRequestConverter(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Photo convert(PhotoChangeRequest request) {
        Photo photo = photoRepository.findById(request.getId()).orElseThrow(
                () -> new MyEntityException("not found Photo with id ='" + request.getId() + "'."));
        return doConvert(photo, request);
    }
}
