package com.mistiq.converter.photo;

import com.mistiq.controller.request.create.PhotoCreateRequest;
import com.mistiq.domain.entity.Photo;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class PhotoConverter<S, T> implements Converter<S, T> {
    protected Photo doConvert(Photo photo, PhotoCreateRequest request) {

        photo.setName(request.getName());
        photo.setDescription(request.getDescription());
        photo.setChanged(new Timestamp(new Date().getTime()));

        return photo;
    }
}
