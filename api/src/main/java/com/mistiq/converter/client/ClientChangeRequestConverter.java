package com.mistiq.converter.client;

import com.mistiq.controller.request.change.ClientChangeRequest;
import com.mistiq.domain.entity.User;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.UserRepository;
import com.mistiq.util.MyConverters;
import org.springframework.stereotype.Component;

@Component
public class ClientChangeRequestConverter extends ClientConverter<ClientChangeRequest, User> {

    private final UserRepository userRepository;

    public ClientChangeRequestConverter(MyConverters converterService, UserRepository userRepository) {
        super(converterService);
        this.userRepository = userRepository;
    }

    @Override
    public User convert(ClientChangeRequest request) {
        User user = userRepository.findById(request.getId()).orElseThrow(
                () -> new MyEntityException
                        (String.format("There is no user with id = %d", request.getId())));
        return doConvert(request, user);
    }

}
