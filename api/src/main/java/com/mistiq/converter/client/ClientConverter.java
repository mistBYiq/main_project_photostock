package com.mistiq.converter.client;

import com.mistiq.controller.request.create.ClientCreateRequest;
import com.mistiq.domain.Address;
import com.mistiq.domain.entity.User;
import com.mistiq.util.MyConverters;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class ClientConverter<S, T> implements Converter<S, T> {

    private final MyConverters converterService;

    public ClientConverter(MyConverters converterService) {
        this.converterService = converterService;
    }

    protected User doConvert(ClientCreateRequest request, User client) {

        client.setName(request.getName());
        client.setSurname(request.getSurname());

        client.setPhone(request.getPhone());
        client.setEmail(request.getEmail());
        client.setGender(request.getGender());
        client.setDateBirth(converterService.convertStringToDate(request.getDateBirth()));

        Address address = new Address();
        address.setCountry(request.getCountry());
        address.setCity(request.getCity());
        address.setStreet(request.getStreet());
        address.setPostalCode(request.getPostalCode());
        client.setAddress(address);

        client.setChanged(new Timestamp(new Date().getTime()));

        return client;
    }

}
