package com.mistiq.converter.order;

import com.mistiq.controller.request.create.OrderCreateRequest;
import com.mistiq.domain.OrderStatus;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Order;
import com.mistiq.domain.entity.Photo;
import com.mistiq.domain.entity.User;
import com.mistiq.service.LicenseService;
import com.mistiq.service.PhotoService;
import com.mistiq.service.UserService;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class OrderCreateConverter extends OrderConverter<OrderCreateRequest, Order> {

    private final UserService userService;

    private final LicenseService licenseService;

    private final PhotoService photoService;

    public OrderCreateConverter(UserService userService,
                                LicenseService licenseService,
                                PhotoService photoService) {
        this.userService = userService;
        this.licenseService = licenseService;
        this.photoService = photoService;
    }

    @Override
    public Order convert(OrderCreateRequest request) {

        License license = licenseService.findLicenseById(request.getLicenseId());
        User user = userService.findUserById(request.getUserId());
        User author = userService.findUserById(license.getPhoto().getAuthor().getId());
        Photo photo = photoService.findPhotoById(license.getPhoto().getId());

        Order order = new Order();
        order.setUser(user);
        order.setLicense(license);
        order.setTotalPrice(license.getPrice());
        order.setPhoto(photo);
        order.setAuthor(author);
        order.setOrderStatus(OrderStatus.NOT_PAID);
        order.setCreated(new Timestamp(new Date().getTime()));
        order.setChanged(new Timestamp(new Date().getTime()));
        order.setIsDeleted(false);

        return doConvert(order, request);
    }
}
