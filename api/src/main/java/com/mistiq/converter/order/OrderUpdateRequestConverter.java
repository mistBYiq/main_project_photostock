package com.mistiq.converter.order;

import com.mistiq.controller.request.change.OrderChangeRequest;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Order;
import com.mistiq.domain.entity.Photo;
import com.mistiq.domain.entity.User;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.OrderRepository;
import com.mistiq.service.LicenseService;
import com.mistiq.service.PhotoService;
import com.mistiq.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class OrderUpdateRequestConverter extends OrderConverter<OrderChangeRequest, Order> {

    private final LicenseService licenseService;

    private final OrderRepository orderRepository;

    private final UserService userService;

    private final PhotoService photoService;

    public OrderUpdateRequestConverter(LicenseService licenseService,
                                       OrderRepository orderRepository,
                                       UserService userService,
                                       PhotoService photoService) {
        this.licenseService = licenseService;
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.photoService = photoService;
    }

    @Override
    public Order convert(OrderChangeRequest request) {

        Order order = orderRepository.findById(request.getId())
                .orElseThrow(
                        () -> new MyEntityException("Order does not exist (converter update)"));

        License license = licenseService.findLicenseById(request.getLicenseId());
        User user = userService.findUserById(request.getUserId());
        User author = userService.findUserById(license.getPhoto().getAuthor().getId());
        Photo photo = photoService.findPhotoById(license.getPhoto().getId());

        order.setUser(user);
        order.setTotalPrice(license.getPrice());
        order.setLicense(license);
        order.setPhoto(photo);
        order.setAuthor(author);

        return doConvert(order, request);
    }
}
