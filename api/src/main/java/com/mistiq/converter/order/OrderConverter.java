package com.mistiq.converter.order;

import com.mistiq.controller.request.create.OrderCreateRequest;
import com.mistiq.domain.entity.Order;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class OrderConverter<S, T> implements Converter<S, T> {
    protected Order doConvert(Order order, OrderCreateRequest request) {

        order.setChanged(new Timestamp(new Date().getTime()));

        return order;
    }
}
