package com.mistiq.converter.user;

import com.mistiq.controller.request.create.UserCreateRequest;
import com.mistiq.domain.Credentials;
import com.mistiq.domain.RoleName;
import com.mistiq.domain.UserStatus;
import com.mistiq.domain.entity.Role;
import com.mistiq.domain.entity.User;
import com.mistiq.util.MyConverters;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class UserCreateRequestConverter extends UserConverter<UserCreateRequest, User> {

    private final PasswordEncoder passwordEncoder;

    public UserCreateRequestConverter(MyConverters converterService, PasswordEncoder passwordEncoder) {
        super(converterService);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User convert(UserCreateRequest request) {
        User user = new User();

        Credentials credentials = new Credentials();
        credentials.setLogin(request.getLogin());
        credentials.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setCredential(credentials);

        user.setCreated(new Timestamp(new Date().getTime()));
        user.setChanged(new Timestamp(new Date().getTime()));

        user.setIsDeleted(false);
        user.setStatus(UserStatus.ACTIVE);
        user.setBalance(0);

        Role role = new Role();
        role.setName(RoleName.ROLE_CLIENT);
        role.setUser(user);
        user.setRole(role);

        return doConvert(request, user);
    }
}
