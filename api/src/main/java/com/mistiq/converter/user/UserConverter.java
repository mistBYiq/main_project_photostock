package com.mistiq.converter.user;

import com.mistiq.controller.request.create.UserCreateRequest;
import com.mistiq.domain.Address;
import com.mistiq.domain.entity.User;
import com.mistiq.util.MyConverters;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class UserConverter<S, T> implements Converter<S, T> {

    private final MyConverters converterService;

    protected UserConverter(MyConverters converterService) {
        this.converterService = converterService;
    }

    protected User doConvert(UserCreateRequest request, User user) {

        user.setName(request.getName());
        user.setSurname(request.getSurname());

        user.setDateBirth(converterService.convertStringToDate(request.getDateBirth()));
        user.setPhone(request.getPhone());
        user.setEmail(request.getEmail());
        user.setGender(request.getGender());
        user.setBalance(request.getBalance());

        Address address = new Address();
        address.setCountry(request.getCountry());
        address.setCity(request.getCity());
        address.setStreet(request.getStreet());
        address.setPostalCode(request.getPostalCode());
        user.setAddress(address);

        user.setChanged(new Timestamp(new Date().getTime()));

        return user;
    }
}
