package com.mistiq.converter.caterory;

import com.mistiq.controller.request.create.CategoryCreateRequest;
import com.mistiq.domain.entity.Category;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class CategoryConverter<S, T> implements Converter<S, T> {

    protected Category doConvert(Category category, CategoryCreateRequest request) {

        category.setName(request.getName());
        category.setChanged(new Timestamp(new Date().getTime()));

        return category;
    }
}
