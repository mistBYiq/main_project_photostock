package com.mistiq.converter.caterory;

import com.mistiq.controller.request.create.CategoryCreateRequest;
import com.mistiq.domain.CategoryName;
import com.mistiq.domain.entity.Category;
import com.mistiq.domain.entity.Photo;
import com.mistiq.service.PhotoService;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class CategoryCreateConverter extends CategoryConverter<CategoryCreateRequest, Category> {

    private final PhotoService photoService;

    public CategoryCreateConverter(PhotoService photoService) {
        this.photoService = photoService;
    }

    @Override
    public Category convert(CategoryCreateRequest request) {

        Photo photo = photoService.findPhotoById(request.getPhotoId());

        Category category = new Category();
        category.setName(CategoryName.NOT_SELECTED);
        category.setPhoto(photo);
        category.setCreated(new Timestamp(new Date().getTime()));
        category.setIsDeleted(false);

        return doConvert(category, request);
    }
}
