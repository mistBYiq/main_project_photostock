package com.mistiq.converter.caterory;

import com.mistiq.controller.request.change.CategoryChangeRequest;
import com.mistiq.domain.entity.Category;
import com.mistiq.domain.entity.Photo;
import com.mistiq.service.CategoryService;
import com.mistiq.service.PhotoService;
import org.springframework.stereotype.Component;

@Component
public class CategoryUpdateRequestConverter extends CategoryConverter<CategoryChangeRequest, Category> {

    private final CategoryService categoryService;
    private final PhotoService photoService;

    public CategoryUpdateRequestConverter(CategoryService categoryService, PhotoService photoService) {
        this.categoryService = categoryService;
        this.photoService = photoService;
    }

    @Override
    public Category convert(CategoryChangeRequest request) {
        Category category = categoryService.findCategoryById(request.getId());
        Photo photo = photoService.findPhotoById(request.getPhotoId());

        category.setPhoto(photo);

        return doConvert(category, request);
    }
}
