package com.mistiq.converter.license;

import com.mistiq.controller.request.change.LicenseChangeRequest;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Photo;
import com.mistiq.service.LicenseService;
import com.mistiq.service.PhotoService;
import org.springframework.stereotype.Component;

@Component
public class LicenseUpdateRequestConverter extends LicenseConverter<LicenseChangeRequest, License> {

    private final LicenseService licenseService;
    private final PhotoService photoService;

    public LicenseUpdateRequestConverter(LicenseService licenseService, PhotoService photoService) {
        this.licenseService = licenseService;
        this.photoService = photoService;
    }

    @Override
    public License convert(LicenseChangeRequest request) {

        License license = licenseService.findLicenseById(request.getId());

        Photo photo = photoService.findPhotoById(request.getPhotoId());
        license.setPhoto(photo);

        return doConvert(license, request);
    }
}
