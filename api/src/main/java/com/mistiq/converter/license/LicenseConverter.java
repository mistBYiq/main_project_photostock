package com.mistiq.converter.license;

import com.mistiq.controller.request.create.LicenseCreateRequest;
import com.mistiq.domain.entity.License;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class LicenseConverter<S, T> implements Converter<S, T> {
    protected License doConvert(License license, LicenseCreateRequest request) {

        license.setLicenseType(request.getLicenseType());
        license.setPrice(request.getLicenseType().getRepresentation());
        license.setChanged(new Timestamp(new Date().getTime()));

        return license;
    }
}
