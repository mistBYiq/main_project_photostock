package com.mistiq.converter.license;

import com.mistiq.controller.request.create.LicenseCreateRequest;
import com.mistiq.domain.entity.License;
import com.mistiq.domain.entity.Photo;
import com.mistiq.service.PhotoService;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class LicenseCreateConverter extends LicenseConverter<LicenseCreateRequest, License> {

    private final PhotoService photoService;

    public LicenseCreateConverter(PhotoService photoService) {
        this.photoService = photoService;
    }

    @Override
    public License convert(LicenseCreateRequest request) {

        Photo photo = photoService.findPhotoById(request.getPhotoId());

        License license = new License();
        license.setPhoto(photo);
        license.setCreated(new Timestamp(new Date().getTime()));
        license.setIsDeleted(false);

        return doConvert(license, request);
    }
}
