package com.mistiq.converter.keyword;

import com.mistiq.controller.request.create.KeywordCreateRequest;
import com.mistiq.domain.entity.Keyword;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.util.Date;

public abstract class KeywordConverter<S, T> implements Converter<S, T> {
    protected Keyword doConvert(Keyword keyword, KeywordCreateRequest request) {

        keyword.setName(request.getName());
        keyword.setChanged(new Timestamp(new Date().getTime()));

        return keyword;
    }
}
