package com.mistiq.converter.keyword;

import com.mistiq.controller.request.change.KeywordChangeRequest;
import com.mistiq.domain.entity.Keyword;
import com.mistiq.service.KeywordService;
import org.springframework.stereotype.Component;

@Component
public class KeywordUpdateRequestConverter extends KeywordConverter<KeywordChangeRequest, Keyword> {

    private final KeywordService keywordService;

    public KeywordUpdateRequestConverter(KeywordService keywordService) {
        this.keywordService = keywordService;
    }

    @Override
    public Keyword convert(KeywordChangeRequest request) {
        Keyword keyword = keywordService.findKeywordById(request.getId());

        return doConvert(keyword, request);
    }
}
