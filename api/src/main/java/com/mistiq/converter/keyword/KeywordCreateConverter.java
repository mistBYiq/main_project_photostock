package com.mistiq.converter.keyword;

import com.mistiq.controller.request.create.KeywordCreateRequest;
import com.mistiq.domain.entity.Keyword;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class KeywordCreateConverter extends KeywordConverter<KeywordCreateRequest, Keyword> {

    @Override
    public Keyword convert(KeywordCreateRequest request) {
        Keyword keyword = new Keyword();

        keyword.setCreated(new Timestamp(new Date().getTime()));
        keyword.setIsDeleted(false);

        return doConvert(keyword, request);
    }
}
