package com.mistiq;

import com.mistiq.config.AmazonConfig;
import com.mistiq.config.ApplicationBeans;
import com.mistiq.config.MailConfig;
import com.mistiq.config.PersistenceContextBeansConfiguration;
import com.mistiq.config.SwaggerConfig;
import com.mistiq.security.configuration.JwtTokenConfig;
import com.mistiq.security.configuration.WebSecurityConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = "com.mistiq")
@EnableCaching
@EnableAspectJAutoProxy(exposeProxy = true)
@Import({
        ApplicationBeans.class,
        SwaggerConfig.class,
        MailConfig.class,
        AmazonConfig.class,
        WebSecurityConfiguration.class,
        JwtTokenConfig.class,
        PersistenceContextBeansConfiguration.class})
public class SpringBootApplicationStarter {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplicationStarter.class, args);
    }
}
