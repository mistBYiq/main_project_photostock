package com.mistiq.exception;

public class MyEntityException extends RuntimeException {

    public MyEntityException() {
        super();
    }

    public MyEntityException(String message) {
        super(message);
    }
}
