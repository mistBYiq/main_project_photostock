package com.mistiq.service;

import com.mistiq.domain.CategoryName;
import com.mistiq.domain.entity.Category;
import com.mistiq.domain.entity.Photo;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    Category save(Category category);

    List<Category> findAll();

    Optional<Category> findById(Long id);

    Category findCategoryById(Long id);

    void delete(Long id);

    void deleteCategory(Long id);

    List<Category> search(CategoryName name);

    Category findCategoryByPhoto(Photo photo);
}
