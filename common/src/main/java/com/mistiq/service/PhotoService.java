package com.mistiq.service;

import com.mistiq.domain.CategoryName;
import com.mistiq.domain.LicenseType;
import com.mistiq.domain.entity.Photo;

import java.util.List;
import java.util.Optional;

public interface PhotoService {

    Photo save(Photo photo);

    List<Photo> findAll();

    Optional<Photo> findById(Long id);

    Photo findPhotoById(Long id);

    void delete(Long id);

    void deletePhoto(Long id);

    List<Photo> search(String query);

    List<Photo> findPhotosByAuthorId(Long id);

    int addKeywordPhoto(Long photoId, Long keywordId);

    Photo checkedNewPhoto(Long photoId);

    List<Photo> findPhotosByIsNewTrue();

    List<Photo> findAllPhotosForClient();

    List<Photo> findPhotosByLicenseTypeForClient(LicenseType licenseType);

    List<Photo> findPhotosByCategoryNameForClient(CategoryName categoryName);

    List<Photo> findPhotosByKeywordNameForClient(String keywordName);

    List<Photo> findPhotosByNameForClient(String name);

    void deleteAuthorPhotos(Long authorId);

    List<Photo> findByNameAndDescriptionCriteria(String name, String description);
}
