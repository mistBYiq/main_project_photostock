package com.mistiq.service;

import com.mistiq.domain.entity.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {

    Role save(Role role);

    List<Role> findAll();

    Optional<Role> findById(Long id);

    Role findRoleById(Long id);

    void delete(Long id);

    void deleteRole(Long id);

    Role findRoleByUserId(Long userId);

    List<Role> findUserRoles(Long userId);

    boolean isAuthor(Long userId);

    boolean isClient(Long userId);
}
