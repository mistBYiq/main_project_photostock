package com.mistiq.service;

import com.mistiq.domain.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User save(User user);

    List<User> findAll();

    Optional<User> findOptionalUserById(Long id);

    User findUserById(Long id);

    void delete(Long id);

    void deleteUser(Long id);

    List<User> search(String query);

    List<User> findAllUsersWithRoleAdmin();

    List<User> findAllWithStatusBanned();

    User findByEmail(String email);

    void banByEmail(String email);

    void unbanByEmail(String email);

    Optional<User> findByCredentialLogin(String login);

    User findUserByLogin(String login);

    void transfer(Long clientId, Long authorId, Integer price);

    void deleteAuthor(Long authorId);

    boolean isDeleted(Long id);

    boolean isActive(Long id);
}