package com.mistiq.service;

import com.mistiq.domain.entity.LinkKeywordPhoto;

public interface LinkKeywordPhotoService {

    LinkKeywordPhoto findByKeywordAndPhoto(Long keywordId, Long photoId);
}
