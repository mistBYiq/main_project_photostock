package com.mistiq.service;

import com.mistiq.domain.entity.License;

import java.util.List;
import java.util.Optional;

public interface LicenseService {

    License save(License license);

    List<License> findAll();

    Optional<License> findById(Long id);

    License findLicenseById(Long id);

    void delete(Long id);

    void deleteLicense(Long id);

    List<License> search(Integer query);

    List<License> findLicensesByPhotoId(Long photoId);

}
