package com.mistiq.service;


import com.mistiq.domain.entity.Order;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    Order save(Order order);

    List<Order> findAll();

    Optional<Order> findById(Long id);

    Order findOrderById(Long id);

    void delete(Long id);

    void deleteOrder(Long id);

    List<Order> findApprovedOrdersByClientId(Long userId);

    List<Order> findNotPaidOrdersByClientId(Long userId);

    Order findOrderByUserIdAndId(Long userId, Long id);

    boolean findOrderByUserIdAndLicenseId(Long userId, Long licenseId);
}
