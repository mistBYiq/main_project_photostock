package com.mistiq.service;

import com.mistiq.domain.entity.Keyword;

import java.util.List;
import java.util.Optional;

public interface KeywordService {

    Keyword save(Keyword keyword);

    List<Keyword> findAll();

    Optional<Keyword> findById(Long id);

    Keyword findKeywordById(Long id);

    void delete(Long id);

    void deleteKeyword(Long id);

    List<Keyword> search(String query);

    List<Keyword> findKeywordsByPhotoId(Long id);

    Optional<Keyword> findKeywordByName(String name);

    Keyword findKeywordByIdWithCache(Long id);
}
