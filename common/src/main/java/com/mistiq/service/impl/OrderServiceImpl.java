package com.mistiq.service.impl;

import com.mistiq.domain.entity.Order;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.OrderRepository;
import com.mistiq.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    // @Transactional
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    @Override
    public Order findOrderById(Long id) {
        return orderRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found Order with id ='" + id + "'."));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteOrder(Long id) {
        findOrderById(id);
        orderRepository.deleteById(id);
    }

    @Override
    public List<Order> findApprovedOrdersByClientId(Long userId) {
        return orderRepository.findApprovedOrdersByClientId(userId);
    }

    @Override
    public List<Order> findNotPaidOrdersByClientId(Long userId) {
        return orderRepository.findNotPaidOrdersByClientId(userId);
    }

    @Override
    public Order findOrderByUserIdAndId(Long userId, Long id) {
        return orderRepository.findOrderByUserIdAndId(userId, id);
    }

    @Override
    public boolean findOrderByUserIdAndLicenseId(Long userId, Long licenseId) {
        return orderRepository.findOrderByUserIdAndLicenseId(userId, licenseId).isPresent();
    }
}
