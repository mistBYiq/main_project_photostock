package com.mistiq.service.impl;

import com.mistiq.domain.CategoryName;
import com.mistiq.domain.entity.Category;
import com.mistiq.domain.entity.Photo;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.CategoryRepository;
import com.mistiq.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    @Transactional
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public Category findCategoryById(Long id) {
        return categoryRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found Category with id ='" + id + "'."));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteCategory(Long id) {
        findCategoryById(id);
        categoryRepository.deleteById(id);
    }

    @Override
    public List<Category> search(CategoryName name) {
        return categoryRepository.findAllByName(name);
    }

    @Override
    public Category findCategoryByPhoto(Photo photo) {
        return categoryRepository.findCategoryByPhoto(photo);
    }
}
