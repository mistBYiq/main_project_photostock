package com.mistiq.service.impl;

import com.mistiq.domain.entity.Keyword;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.KeywordRepository;
import com.mistiq.service.KeywordService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class KeywordServiceImpl implements KeywordService {

    private final KeywordRepository keywordRepository;

    public KeywordServiceImpl(KeywordRepository keywordRepository) {
        this.keywordRepository = keywordRepository;
    }

    @Override
    @Transactional
    @CachePut(value = "keywords_cache", key = "#keyword.id")
    public Keyword save(Keyword keyword) {
        return keywordRepository.save(keyword);
    }

    @Override
    public List<Keyword> findAll() {
        return keywordRepository.findAll();
    }

    @Override
    public Optional<Keyword> findById(Long id) {
        return keywordRepository.findById(id);
    }

    @Override
    public Keyword findKeywordById(Long id) {
        return keywordRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found Keyword with id ='" + id + "'."));
    }

    @Override
    @Transactional
    @CacheEvict(value = "keywords_cache", key = "#id")
    public void delete(Long id) {
        keywordRepository.deleteById(id);
    }

    @Override
    @Transactional
    @CacheEvict(value = "keywords_cache", key = "#id")
    public void deleteKeyword(Long id) {
        findKeywordById(id);
        keywordRepository.deleteById(id);
    }

    @Override
    public List<Keyword> search(String name) {
        return keywordRepository.findByNameContaining(name);
    }

    @Override
    public List<Keyword> findKeywordsByPhotoId(Long id) {
        return keywordRepository.findKeywordsByPhotoId(id);
    }

    @Override
    public Optional<Keyword> findKeywordByName(String name) {
        return keywordRepository.findKeywordByName(name);
    }

    @Override
    @Cacheable(value = "keywords_cache", key = "#id")
    public Keyword findKeywordByIdWithCache(Long id) {
        return keywordRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found Keyword with id ='" + id + "'."));
    }
}
