package com.mistiq.service.impl;

import com.mistiq.domain.entity.LinkKeywordPhoto;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.LinkKeywordPhotoRepository;
import com.mistiq.service.LinkKeywordPhotoService;
import org.springframework.stereotype.Service;

@Service
public class LinkKeywordPhotoServiceImpl implements LinkKeywordPhotoService {

    private final LinkKeywordPhotoRepository linkKeywordPhotoRepository;

    public LinkKeywordPhotoServiceImpl(LinkKeywordPhotoRepository linkKeywordPhotoRepository) {
        this.linkKeywordPhotoRepository = linkKeywordPhotoRepository;
    }

    @Override
    public LinkKeywordPhoto findByKeywordAndPhoto(Long keywordId, Long photoId) {
        return linkKeywordPhotoRepository.findByKeywordAndPhoto(keywordId, photoId).orElseThrow(
                () -> new MyEntityException
                        ("not found LinkKeywordPhoto with keyword id ='" + keywordId + "' and photo id =" + photoId)
        );
    }
}
