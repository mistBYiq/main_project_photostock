package com.mistiq.service.impl;

import com.mistiq.domain.UserStatus;
import com.mistiq.domain.entity.User;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.UserRepository;
import com.mistiq.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(User user) {
        //1. Validation layer
        //2. Convert http request params into User object
        //3. Extended calls into DB or external services
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findOptionalUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new MyEntityException("Not found User  with id ='" + id + "'."));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        findUserById(id);
        userRepository.deleteById(id);
    }

    @Override
    public List<User> search(String name) {
        return userRepository.findByNameContaining(name);
    }


    @Override
    public List<User> findAllUsersWithRoleAdmin() {
        return userRepository.findAllUsersWithRoleAdmin();
    }

    @Override
    public List<User> findAllWithStatusBanned() {
        return userRepository.findAllWithStatusBanned();
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findOneByEmail(email).orElseThrow(
                () -> new MyEntityException("Not  found  User with email ='" + email + "'."));
    }

    @Override
    @Transactional
    public void banByEmail(String email) {
        User user = userRepository.findOneByEmail(email).orElseThrow(
                () -> new MyEntityException("Not found User with email ='" + email + "'."));

        user.setStatus(UserStatus.BANNED);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void unbanByEmail(String email) {
        User user = userRepository.findOneByEmail(email).orElseThrow(
                () -> new MyEntityException("Not found User with email ='" + email + "'."));

        user.setStatus(UserStatus.ACTIVE);
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByCredentialLogin(String login) {
        return userRepository.findByCredentialLogin(login);
    }

    @Override
    public User findUserByLogin(String login) {
        return userRepository.findByCredentialLogin(login).orElseThrow(
                () -> new MyEntityException("Not found User with login ='" + login + "'."));
    }

    @Override
    public void transfer(Long clientId, Long authorId, Integer price) {
        User user = userRepository.findById(clientId).orElseThrow(
                () -> new MyEntityException("Not found User with id ='" + clientId + "'.")
        );

        User author = userRepository.findById(authorId).orElseThrow(
                () -> new MyEntityException("Not found Author with id ='" + authorId + "'.")
        );

        Integer clientBalance = user.getBalance() - price;
        Integer authorBalance = author.getBalance() + price;

        if (userRepository.updateBalance(clientId, clientBalance) != 1) {
            throw new MyEntityException("Something went wrong. Exception in transfer method");
        }
        if (userRepository.updateBalance(authorId, authorBalance) != 1) {
            throw new MyEntityException("Something went wrong. Exception in transfer method");
        }
    }

    @Override
    public void deleteAuthor(Long authorId) {
        User author = userRepository.findById(authorId).orElseThrow(
                () -> new MyEntityException("Not  found Author with id ='" + authorId + "'."));

        author.setIsDeleted(true);
        author.setChanged(new Timestamp(System.currentTimeMillis()));

        userRepository.save(author);
    }

    @Override
    public boolean isDeleted(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new MyEntityException("Not   found User with id ='" + id + "'.")
        );
        return user.getIsDeleted();
    }

    @Override
    public boolean isActive(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new MyEntityException("Not found User with id ='" + id + "'.")
        );
        return user.getStatus() == UserStatus.ACTIVE;
    }
}
