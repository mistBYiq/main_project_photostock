package com.mistiq.service.impl;

import com.mistiq.domain.RoleName;
import com.mistiq.domain.entity.Role;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.RoleRepository;
import com.mistiq.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Optional<Role> findById(Long id) {
        return roleRepository.findById(id);
    }

    @Override
    public Role findRoleById(Long id) {
        return roleRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found Role with id ='" + id + "'."));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        roleRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteRole(Long id) {
        findRoleById(id);
        roleRepository.deleteById(id);
    }

    @Override
    public Role findRoleByUserId(Long userId) {
        return roleRepository.findRoleByUserId(userId).orElseThrow(
                () -> new MyEntityException("not found  Role with User id = " + userId));
    }

    @Override
    public List<Role> findUserRoles(Long userId) {
        List<Role> listR = new ArrayList<>();
        Role role;

        Optional<Role> optionalRole = roleRepository.findRoleByUserId(userId);
        if (optionalRole.isPresent()) {
            role = optionalRole.get();
            listR.add(role);
        }
        return listR;
    }

    @Override
    public boolean isAuthor(Long userId) {
        Role role = roleRepository.findRoleByUserId(userId).orElseThrow(
                () -> new MyEntityException("not found Role with User id = " + userId));

        return role.getName() == RoleName.ROLE_AUTHOR;
    }

    @Override
    public boolean isClient(Long userId) {
        Role role = roleRepository.findRoleByUserId(userId).orElseThrow(
                () -> new MyEntityException("not found Role with  User id = " + userId));

        return role.getName() == RoleName.ROLE_CLIENT;
    }
}
