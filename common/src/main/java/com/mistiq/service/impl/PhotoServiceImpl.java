package com.mistiq.service.impl;

import com.mistiq.domain.CategoryName;
import com.mistiq.domain.LicenseType;
import com.mistiq.domain.entity.Photo;
import com.mistiq.exception.MyEntityException;
import com.mistiq.specification.PhotoSpecification;
import com.mistiq.repository.impl.PhotoRepository;
import com.mistiq.service.PhotoService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;

    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    @Transactional
    @CachePut(value = "photos_cache", key = "#photo.id")
    public Photo save(Photo photo) {
        return photoRepository.save(photo);
    }

    @Override
    public List<Photo> findAll() {
        return photoRepository.findAll();
    }

    @Override
    public Optional<Photo> findById(Long id) {
        return photoRepository.findById(id);
    }

    @Override
    @Cacheable(value = "photos_cache", key = "#id")
    public Photo findPhotoById(Long id) {
        return photoRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found Photo with id ='" + id + "'."));
    }

    @Override
    @Transactional
    @CacheEvict(value = "photos_cache", key = "#id")
    public void delete(Long id) {
        photoRepository.deleteById(id);
    }

    @Override
    @Transactional
    @CacheEvict(value = "photos_cache", key = "#id")
    public void deletePhoto(Long id) {
        findPhotoById(id);
        photoRepository.deleteById(id);
    }

    @Override
    public List<Photo> search(String query) {
        return photoRepository.findByNameContaining(query);
    }

    @Override
    public List<Photo> findPhotosByAuthorId(Long authorId) {
        return photoRepository.findPhotosByAuthorId(authorId);
    }

    @Override
    public int addKeywordPhoto(Long photoId, Long keywordId) {
        return photoRepository.addKeywordPhoto(photoId, keywordId);
    }

    @Override
    @Transactional
    public Photo checkedNewPhoto(Long photoId) {
        Photo photo = photoRepository.findById(photoId).orElseThrow(
                () -> new MyEntityException("not found Photo with id ='" + photoId + "'."));
        photo.setIsNew(false);

        return photo;
    }

    @Override
    public List<Photo> findPhotosByIsNewTrue() {
        return photoRepository.findPhotosByIsNewTrueOrderByCreatedAsc();
    }

    @Override
    public List<Photo> findAllPhotosForClient() {
        return photoRepository.findPhotosByIsDeletedFalseAndIsNewFalse();
    }

    @Override
    public List<Photo> findPhotosByLicenseTypeForClient(LicenseType licenseType) {
        return photoRepository.findPhotosByLicenseType(licenseType.toString());
    }

    @Override
    public List<Photo> findPhotosByCategoryNameForClient(CategoryName categoryName) {
        return photoRepository.findPhotosByCategoryName(categoryName.getRepresentation());
    }

    @Override
    public List<Photo> findPhotosByKeywordNameForClient(String keywordName) {
        return photoRepository.findPhotosByKeywordName(keywordName);
    }

    @Override
    public void deleteAuthorPhotos(Long authorId) {
        List<Photo> photos = photoRepository.findPhotosByAuthorId(authorId);
        for (Photo photo : photos) {
            photo.setIsDeleted(true);
            photo.setChanged(new Timestamp(System.currentTimeMillis()));
            photoRepository.save(photo);
        }
    }

    @Override
    public List<Photo> findPhotosByNameForClient(String name) {
        return photoRepository.findPhotosByNameForClient(name);
    }

    @Override
    public List<Photo> findByNameAndDescriptionCriteria(String name, String description) {
        Specification<Photo> specification = Specification.where(PhotoSpecification.hasName(name).
                and(PhotoSpecification.descriptionContains(description)));
        return photoRepository.findAll(specification);
    }
}
