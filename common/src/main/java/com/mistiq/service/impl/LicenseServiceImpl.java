package com.mistiq.service.impl;

import com.mistiq.domain.entity.License;
import com.mistiq.exception.MyEntityException;
import com.mistiq.repository.impl.LicenseRepository;
import com.mistiq.service.LicenseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class LicenseServiceImpl implements LicenseService {

    private final LicenseRepository licenseRepository;

    public LicenseServiceImpl(LicenseRepository licenseRepository) {
        this.licenseRepository = licenseRepository;
    }

    @Override
    @Transactional
    public License save(License license) {
        return licenseRepository.save(license);
    }

    @Override
    public List<License> findAll() {
        return licenseRepository.findAll();
    }

    @Override
    public Optional<License> findById(Long id) {
        return licenseRepository.findById(id);
    }

    @Override
    public License findLicenseById(Long id) {
        return licenseRepository.findById(id).orElseThrow(
                () -> new MyEntityException("not found License with id ='" + id + "'."));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        licenseRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteLicense(Long id) {
        findLicenseById(id);
        licenseRepository.deleteById(id);
    }

    @Override
    public List<License> search(Integer price) {
        return licenseRepository.findAllByPrice(price);
    }

    @Override
    public List<License> findLicensesByPhotoId(Long photoId) {
        return licenseRepository.findLicensesByPhotoId(photoId);
    }

}
