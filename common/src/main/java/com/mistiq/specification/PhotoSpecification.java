package com.mistiq.specification;

import com.mistiq.domain.entity.Photo;
import com.mistiq.domain.entity.Photo_;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class PhotoSpecification {

    public static Specification<Photo> hasName(final String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Photo_.NAME), name);
    }

    public static Specification<Photo> descriptionContains(String description) {
        return (book, cq, cb) -> cb.like(book.get(Photo_.DESCRIPTION), "%" + description + "%");
    }

}


