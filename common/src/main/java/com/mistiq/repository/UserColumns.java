package com.mistiq.repository;

public interface UserColumns {
    String ID = "id";
    String USER_ROLES = "user_role_id";
    String NAME = "name";
    String SURNAME = "surname";
    String EMAIL = "email";
    String PHONE = "phone";
    String LOGIN = "login";
    String PASSWORD = "password";
    String CREATED = "created";
    String CHANGED = "changed";
    String BALANCE = "balance";
    String STATUS = "status";
    String GENDER = "gender";
}
