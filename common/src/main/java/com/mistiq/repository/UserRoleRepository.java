package com.mistiq.repository;

import com.mistiq.domain.entity.Role;

import java.util.List;

public interface UserRoleRepository extends CrudRepository<Long, Role> {
    List<Role> findUserRoles(Long userId);
}
