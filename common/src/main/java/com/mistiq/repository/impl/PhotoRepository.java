package com.mistiq.repository.impl;

import com.mistiq.domain.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long>, JpaSpecificationExecutor<Photo> {

    List<Photo> findPhotosByAuthorId(Long id);

    List<Photo> findByNameContaining(String name);

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = SQLException.class)
    @Modifying
    @Query(nativeQuery = true, value = "insert into l_keyword_photo(keyword_id, photo_id) values (:keyword_id, :photo_id)")
    int addKeywordPhoto(@Param("photo_id") Long photoId, @Param("keyword_id") Long keywordId);

    @Query(nativeQuery = true, value = "select * from m_photos where is_new = true")
    List<Photo> findPhotosNew();

    List<Photo> findPhotosByIsNewTrueOrderByCreatedAsc();

    List<Photo> findPhotosByIsDeletedFalseAndIsNewFalse();

    @Query(nativeQuery = true, value = "select * from func_photos_search_by_license_type(:license)")
    List<Photo> findPhotosByLicenseType(@Param("license") String licenseType);

    @Query(nativeQuery = true, value = "select * from func_photos_search_by_category_name(:category)")
    List<Photo> findPhotosByCategoryName(@Param("category") String categoryName);

    @Query(nativeQuery = true, value = "select * from func_photos_search_by_keyword_name(:keyword)")
    List<Photo> findPhotosByKeywordName(@Param("keyword") String keywordName);

    @Query(value = "select p from Photo p where p.name like concat(:name , '%') and p.isNew = false and p.isDeleted = false")
    List<Photo> findPhotosByNameForClient(@Param("name") String name);
}
