package com.mistiq.repository.impl;

import com.mistiq.domain.entity.Role;
import com.mistiq.repository.UserRoleRepository;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

//@Repository
public class UserRoleRepositoryImpl implements UserRoleRepository {

    private static final Logger log = Logger.getLogger(UserRoleRepositoryImpl.class);

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRoleRepositoryImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Role> findUserRoles(Long userId) {
        return Collections.emptyList();
    }

    @Override
    public Role save(Role object) {
        return null;
    }

    @Override
    public List<Role> findAll() {
        return null;
    }

    @Override
    public Role findById(Long key) {
        return null;
    }

    @Override
    public Optional<Role> findOne(Long key) {
        return Optional.empty();
    }

    @Override
    public Role update(Role object) {
        return null;
    }

    @Override
    public Long delete(Role object) {
        return null;
    }
}
