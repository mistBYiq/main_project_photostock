package com.mistiq.repository.impl;

import com.mistiq.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByCredentialLogin(String login);

    List<User> findByNameContaining(String name);

    @Query(value = "select * from m_users join m_roles on m_roles.user_id = m_users.id where m_roles.name = 'ROLE_ADMIN'",
            nativeQuery = true)
    List<User> findAllUsersWithRoleAdmin();

    @Query(value = "select u from User u where u.status = 'BANNED'")
    List<User> findAllWithStatusBanned();

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = SQLException.class)
    @Modifying
    @Query(nativeQuery = true, value = "update m_users set balance = :price where id = :user_id")
    int updateBalance(@Param("user_id") Long userId, @Param("price") Integer price);

    Optional<User> findOneByEmail(String email);
}
