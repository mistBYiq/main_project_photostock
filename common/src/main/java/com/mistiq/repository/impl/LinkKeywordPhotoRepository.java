package com.mistiq.repository.impl;

import com.mistiq.domain.entity.LinkKeywordPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LinkKeywordPhotoRepository extends JpaRepository<LinkKeywordPhoto, Long> {

    Optional<LinkKeywordPhoto> findByKeywordAndPhoto(Long keywordId, Long photoId);
}
