package com.mistiq.repository.impl;

import com.mistiq.domain.entity.User;
import com.mistiq.domain.Gender;
import com.mistiq.domain.UserStatus;
import com.mistiq.repository.UserColumns;
import com.mistiq.repository.UserCustomRepository;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

//@Slf4j
//@Repository
public class UserRepositoryJdbcTemplateImpl implements UserCustomRepository {

    private static final org.apache.log4j.Logger log = Logger.getLogger(UserRepositoryJdbcTemplateImpl.class);
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<User> search(String query) {
        log.info("invoking search method");
        log.info(query);
        return jdbcTemplate.query("select * from m_users where name like ?", new Object[]{query}, this::getUserRowMapper);
    }

    @Override
    public User save(User entity) {
        final String createQuery = "insert into m_users ( name, surname, date_birth, email, phone, login, password, created, changed, is_deleted, balance, status, gender) " +
                "values (:name, :surname, :date_birth, :email, :phone, :login, :password, :created, :changed, :isDeleted, :balance, :status, :gender);";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("name", entity.getName());
        params.addValue("surname", entity.getSurname());
        params.addValue("email", entity.getEmail());
        params.addValue("phone", entity.getPhone());
        params.addValue("login", entity.getCredential().getLogin());
        params.addValue("password", entity.getCredential().getPassword());
        params.addValue("created", entity.getCreated());
        params.addValue("changed", entity.getChanged());
        params.addValue("balance", entity.getBalance());
        params.addValue("status", entity.getStatus());
        params.addValue("gender", entity.getGender());
        params.addValue("isDeleted", entity.getIsDeleted());

        namedParameterJdbcTemplate.update(createQuery, params, keyHolder, new String[]{"id"});

        long createdUserId = Objects.requireNonNull(keyHolder.getKey()).longValue();

        return findById(createdUserId);
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("select * from m_users", this::getUserRowMapper);
    }

    @Override
    public User findById(Long key) {
//        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
//        mapSqlParameterSource.addValue("userId", key);
//
//        return namedParameterJdbcTemplate.queryForObject("select * from m_users where id = :userId", mapSqlParameterSource, this::getUserRowMapper);

        return jdbcTemplate.queryForObject("select * from m_users where id=?", new Object[]{key}, this::getUserRowMapper);
    }

    @Override
    public Optional<User> findOne(Long key) {
        return Optional.empty();
    }

    @Override
    public User update(User object) {
        return null;
    }

    @Override
    public Long delete(User object) {
        return null;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return Optional.of(jdbcTemplate.queryForObject("select * from m_users where login = ?", new Object[]{login}, this::getUserRowMapper));
    }

    private User getUserRowMapper(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(UserColumns.ID));
        user.setName(resultSet.getString(UserColumns.NAME));
        user.setSurname(resultSet.getString(UserColumns.SURNAME));
        user.setEmail(resultSet.getString(UserColumns.EMAIL));
        user.setPhone(resultSet.getString(UserColumns.PHONE));
        user.setCreated(resultSet.getTimestamp(UserColumns.CREATED));
        user.setChanged(resultSet.getTimestamp(UserColumns.CHANGED));
        user.setStatus(UserStatus.valueOf(resultSet.getString(UserColumns.STATUS)));
        user.setGender(Gender.valueOf(resultSet.getString(UserColumns.GENDER)));

        return user;
    }
}
