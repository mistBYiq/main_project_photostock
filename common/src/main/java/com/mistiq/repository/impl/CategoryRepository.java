package com.mistiq.repository.impl;

import com.mistiq.domain.entity.Category;
import com.mistiq.domain.CategoryName;
import com.mistiq.domain.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findAllByName(CategoryName name);

    Category findCategoryByPhoto(Photo photo);
}
