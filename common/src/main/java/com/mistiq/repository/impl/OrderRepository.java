package com.mistiq.repository.impl;

import com.mistiq.domain.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "select * from orders o where o.user_id = :userId and o.is_deleted = false and o.status = 'APPROVED'",
            nativeQuery = true)
    List<Order> findApprovedOrdersByClientId(@Param("userId") Long userId);

    @Query(value = "select * from orders o where o.user_id = :user_id and o.is_deleted = false and o.status = 'NOT_PAID'",
            nativeQuery = true)
    List<Order> findNotPaidOrdersByClientId(@Param("user_id") Long userId);

    Order findOrderByUserIdAndId(Long userId, Long id);

    Optional<Order> findOrderByUserIdAndLicenseId(Long userId, Long licenseId);
}
