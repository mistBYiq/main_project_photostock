package com.mistiq.repository.impl;

import com.mistiq.domain.entity.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {

    List<License> findAllByPrice(Integer price);

    List<License> findLicensesByPhotoId(Long photoId);
}
