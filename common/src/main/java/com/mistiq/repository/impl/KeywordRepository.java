package com.mistiq.repository.impl;

import com.mistiq.domain.entity.Keyword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KeywordRepository extends JpaRepository<Keyword, Long> {

    List<Keyword> findByNameContaining(String name);

    @Query(value = "select * from func_keywords_search(:id)", nativeQuery = true)
    List<Keyword> findKeywordsByPhotoId(@Param("id") Long id);

    Optional<Keyword> findKeywordByName(String name);
}
