package com.mistiq.repository;

import com.mistiq.domain.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserCustomRepository extends CrudRepository<Long, User> {

    List<User> search(String query);

    Optional<User> findByLogin(String login);
}
