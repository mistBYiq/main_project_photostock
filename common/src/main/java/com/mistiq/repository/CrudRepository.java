package com.mistiq.repository;

import java.util.List;
import java.util.Optional;

/*Generic interface for CRUD operations
 * @param K - primary key of objects
 *        V - object type*/
public interface CrudRepository<K,V> {

    V save(V object);

    List<V> findAll();

    V findById(K key);

    Optional<V> findOne(K key);

    V update(V object);

    K delete(V object);

}
