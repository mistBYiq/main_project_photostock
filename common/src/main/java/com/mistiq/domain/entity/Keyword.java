package com.mistiq.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Set;

@Entity
@Data
@Table(name = "m_keywords")
public class Keyword {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
//    @Pattern.List({
//            @Pattern(regexp = "[A-Z][a-z]*"),
//            @Pattern(regexp = "")
//    })
    @Size(min = 3, max = 30)
    private String name;

    @Column(name = "created", updatable = false)
    private Timestamp created;

    @Column(name = "changed", insertable = false)
    private Timestamp changed;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToMany
    @JoinTable(name = "l_keyword_photo",
            joinColumns = @JoinColumn(name = "keyword_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "photo_id", referencedColumnName = "id"))
    @JsonIgnoreProperties("keywords")
    @JsonBackReference
    private Set<Photo> photos = Collections.emptySet();

}
