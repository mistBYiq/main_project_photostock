package com.mistiq.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.mistiq.domain.LicenseType;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Set;

@Entity
@Data
@Table(name = "m_license")
public class License {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private LicenseType licenseType;

    @Column(name = "price")
    private Integer price;

    @ManyToOne
    @JoinColumn(name = "photo_id")
    @JsonManagedReference
    private Photo photo;

    @Column(name = "created", updatable = false)
    private Timestamp created;

    @Column(name = "changed", insertable = false)
    private Timestamp changed;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @OneToMany(mappedBy = "license", fetch = FetchType.LAZY)
    //@JsonIgnore
    @JsonBackReference
    private Set<Order> orders = Collections.emptySet();
}
