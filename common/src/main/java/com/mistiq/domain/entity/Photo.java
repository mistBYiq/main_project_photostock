package com.mistiq.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"keywords"})
@Table(name = "m_photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 2, max = 50)
    @Column(name = "name")
    private String name;

    @Size(min = 2, max = 200)
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "author_id")
    @JsonIgnore
    //@JsonManagedReference
    private User author;

    @Column(name = "url")
    private String url;

    @Column(name = "preview")
    private String preview;

    @Column(name = "is_new")
    private Boolean isNew;

    @Column(name = "created", updatable = false)
    private Timestamp created;

    @Column(name = "changed", insertable = false)
    private Timestamp changed;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @OneToOne(mappedBy = "photo")
    @JsonBackReference
    private Category category;

    @ManyToMany(mappedBy = "photos", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("photos") //
    @JsonBackReference
    private Set<Keyword> keywords = Collections.emptySet();

    @OneToMany(mappedBy = "photo", fetch = FetchType.LAZY)
    @JsonBackReference
    private Set<License> licenses = Collections.emptySet();

    @OneToMany(mappedBy = "photo", fetch = FetchType.LAZY)
    @JsonBackReference
    private Set<Order> orders = Collections.emptySet();

}
