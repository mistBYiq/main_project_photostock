package com.mistiq.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    @Size(min = 2)
    private String country;

    @Size(min = 2)
    private String city;

    private String postalCode;

    @Size(min = 2)
    private String street;
}
