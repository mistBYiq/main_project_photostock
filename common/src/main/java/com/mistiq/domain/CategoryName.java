package com.mistiq.domain;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum CategoryName {
    ABSTRACTION("ABSTRACTION"),
    ANIMALS("ANIMALS"),
    ART("ART"),
    BACKGROUNDS_AND_TEXTURES("BACKGROUNDS_AND_TEXTURES"),
    BEAUTY_AND_FASHION("BEAUTY_AND_FASHION"),
    BUILDINGS_AND_ATTRACTIONS("BUILDINGS_AND_ATTRACTIONS"),
    EDUCATION("EDUCATION"),
    MEDICINE("MEDICINE"),
    HOLIDAYS("HOLIDAYS"),
    NATURE("NATURE"),
    PEOPLE("PEOPLE"),
    ITEMS("ITEMS"),
    SPACE("SPACE"),
    TRANSPORT("TRANSPORT"),
    NOT_SELECTED("NOT_SELECTED");

    private final String representation;

    CategoryName(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
