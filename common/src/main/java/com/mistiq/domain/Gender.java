package com.mistiq.domain;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum Gender {

    MALE("MALE"),
    FEMALE("FEMALE"),
    NOT_SELECTED("NOT_SELECTED");

    private final String representation;

    Gender(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
