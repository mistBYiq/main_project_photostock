package com.mistiq.domain;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum UserStatus {

    ACTIVE("ACTIVE"),
    BANNED("BANNED");

    private final String representation;

    UserStatus(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
