package com.mistiq.domain;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum OrderStatus {

    NOT_PAID("NOT_PAID"),
    APPROVED("APPROVED");

    private final String representation;

    OrderStatus(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
