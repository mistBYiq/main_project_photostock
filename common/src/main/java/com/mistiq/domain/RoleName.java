package com.mistiq.domain;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum RoleName {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_CLIENT("ROLE_CLIENT"),
    ROLE_AUTHOR("ROLE_AUTHOR");

    private final String representation;

    RoleName(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
