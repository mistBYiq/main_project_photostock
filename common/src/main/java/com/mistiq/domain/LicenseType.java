package com.mistiq.domain;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum LicenseType {

    ROYALTY_FREE(0),
    STANDARD(10),
    EXTENDED(100);

    private final Integer representation;

    LicenseType(Integer representation) {
        this.representation = representation;
    }

    public Integer getRepresentation() {
        return representation;
    }

}
