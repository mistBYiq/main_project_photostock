drop function if exists func_photos_search_by_license_type;

create function func_photos_search_by_license_type(name varchar)
    RETURNS setof m_photos
AS
$$

select p.*
from m_photos p
         join m_license l
              on p.id = l.photo_id and l.name = $1
    where p.is_deleted = false and p.is_new = false;
$$ language SQL;



drop function if exists func_photos_search_by_category_name;

create function func_photos_search_by_category_name(name varchar)
    RETURNS setof m_photos
AS
$$

select p.*
from m_photos p
         join m_category c
              on p.id = c.photo_id and c.name = $1
where p.is_deleted = false and p.is_new = false;
$$ language SQL;



drop function if exists func_photos_search_by_keyword;
create function func_photos_search_by_keyword(name varchar)
    RETURNS setof m_photos
    language sql
as
$$
select k.*
from m_photos p
         join l_keyword_photo lkp
              on k.id = lkp.keyword_id and lkp.photo_id = $1;

$$;

alter function func_keywords_search(bigint) owner to postgres;


drop function if exists func_photos_search_by_keyword;
create function func_photos_search_by_keyword(name varchar)
    RETURNS setof m_photos
    language sql
as
$$
select p.*
from m_photos p
         join l_keyword_photo lkp, m_keywords k
              on p.id = lkp.keyword_id and lkp.name = $1
where p.is_deleted = false and p.is_new = false;
$$ language SQL;

alter function func_keywords_search(bigint) owner to postgres;



INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'Testtring', 'string', '2021-02-25', 'MALE', '93719992', 'satr1ing@gmail.commmm', '$2a$10$tAL6EZItnWelcfDIDsWZA.i4z7OvlN0sLt8zYvPxunLerhaum.7Hi', 'stri1ngasdfghjkl', 'string', 'string', null, '1', '2021-02-25 02:03:53.822000', '2021-02-25 02:03:53.881387', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'Testtring', 'string', '2021-02-25', 'NOT_SELECTED', '93719992', 'satring@gmail.commmm', '$2a$10$tasvb95AdvW1n9VhHETniuxlb1K2V0ztf27FbDrwNMCTUBlxYry3i', 'stringasdfghjkl', 'string', 'string', null, '1', '2021-02-25 02:00:01.483000', '2021-02-25 02:00:01.486405', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'aUthor1', 'author1', null, null, null, '1author@mail.com', 'password12', 'login12', null, null, null, null, '2021-02-23 02:45:27.579000', '2021-02-23 02:45:27.797868', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'Testtring', 'string', '2021-02-25', 'NOT_SELECTED', '93719992', 'string@gmail.commmm', '$2a$10$l.26EmThZhQ4qLV.Cytw2O9xITHZi0s.T9sf.Medp//0gKY3to.0y', 'stringsdfghjkl', 'string', 'string', null, '1', '2021-02-25 01:58:13.292000', '2021-02-25 01:58:13.348208', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'sSString', 'string', null, null, null, 'sstring@mail.com', 'string', 'sstring', null, null, null, null, '2021-02-20 16:04:49.385000', '2021-02-25 23:45:25.250000', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'aUthor1', 'author1', null, null, null, 'author@mail.com', 'password12', 'login1', null, null, null, null, '2021-02-23 02:13:02.517000', '2021-02-23 02:13:02.577174', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'string', 'string', null, null, null, 'string@mail.com', 'string', 'string', null, null, null, null, '2021-02-20 15:50:19.943000', '2021-02-20 15:50:19.968178', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'test', 'test', null, null, null, 'testsw@mail.com', '$2a$10$DkoCp8oTz69CIE/11as/3.w.QoXjIIBXRyP0mwKYEeID6mI7v4LJy', 'test', null, null, null, null, '2021-02-15 02:06:17.294000', '2021-02-15 02:06:17.562524', true, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ('First', 'Surname', null, 'NOT_SELECTED', '9379992', 'first@mail.com', '111', '12345678', null, null, null, null, '2021-02-09 20:14:36.820772', '2021-02-09 20:14:36.820772', false, 'BANNED', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( '1aUthor122', 'author1', null, 'NOT_SELECTED', null, '11au112th4or@mail.com', '$2a$10$Jo6gjvhOCIaB3MHDgpQguu3r4CE/P0QPLcb6qQQJXdu/hJdstUBom', '1l2ogin152', null, null, null, null, '2021-02-23 03:19:18.063000', '2021-02-23 03:19:18.262733', false, 'ACTIVE', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'aUthor122', 'author1', null, null, null, '1au112thor@mail.com', '$2a$10$hYxHxXMCr1zjwZkIeoomM.pZYQEiedSfWxuvUjvDAOn6a8WVhWZx6', '1l2ogin12', null, null, null, null, '2021-02-23 03:14:41.199000', '2021-02-23 03:14:41.399481', true, 'BANNED', 0);
INSERT INTO public.m_users (name, surname, date_birth, gender, phone, email, password, login, country, city, street, postal_code, created, changed, is_deleted, status, balance) VALUES ( 'aUthor1', 'author1', null, 'NOT_SELECTED', null, '1au1thor@mail.com', '$2a$10$xvzbbFtFVXSqafkGi1Jqi.qlkmzgsC80idFNyudrwyMSAa5RvZr7G', '1login12', null, null, null, null, '2021-02-23 02:56:37.323000', '2021-02-23 02:56:37.5145', true, 'BANNED', 0);

INSERT INTO public.m_roles (name) VALUES ('ROLE_USER');
INSERT INTO public.m_roles (name) VALUES ('ROLE_AUTHOR');
INSERT INTO public.m_roles (name) VALUES ('ROLE_USER');
INSERT INTO public.m_roles (name) VALUES ('ROLE_AUTHOR');
INSERT INTO public.m_roles (name) VALUES ('ROLE_AUTHOR');
INSERT INTO public.m_roles (name) VALUES ('ROLE_AUTHOR');
INSERT INTO public.m_roles (name) VALUES ('ROLE_ADMIN');
INSERT INTO public.m_roles (name) VALUES ('ROLE_USER');
INSERT INTO public.m_roles (name) VALUES ('ROLE_USER');
INSERT INTO public.m_roles (name) VALUES ( 'ROLE_USER');
INSERT INTO public.m_roles (name) VALUES ( 'ROLE_ADMIN');
INSERT INTO public.m_roles (name) VALUES ( 'ROLE_USER');

INSERT INTO public.m_license (name, price, created, changed, is_deleted) VALUES ('STANDARD', 10, '2021-02-24 23:22:50.006000', '2021-02-24 23:22:50.047480', false);
INSERT INTO public.m_license (name, price, created, changed, is_deleted) VALUES ('EXTENDED', 100, '2021-02-24 23:25:13.012000', '2021-02-24 23:25:13.070321', false);
INSERT INTO public.m_license (name, price, created, changed, is_deleted) VALUES ('ROYALTY_FREE', 0, '2021-02-24 23:35:15.501000', '2021-02-24 23:35:15.563400', false);
INSERT INTO public.m_license (name, price, created, changed, is_deleted) VALUES ('NOT_SELECTED', 0, '2021-02-24 23:38:16.782000', '2021-02-24 23:38:16.842623', false);
INSERT INTO public.m_license (name, price, created, changed, is_deleted) VALUES ('ROYALTY_FREE', 0, '2021-02-26 12:47:31.781000', '2021-02-26 12:47:31.803282', false);

INSERT INTO public.m_keywords (name, created, changed, is_deleted) VALUES ('Yellow ', '2021-02-27 13:49:37.692640', '2021-02-27 13:49:37.692640', false);
INSERT INTO public.m_keywords (name, created, changed, is_deleted) VALUES ('Lake', '2021-02-27 15:08:49.803314', '2021-02-27 15:08:49.803314', false);
INSERT INTO public.m_keywords (name, created, changed, is_deleted) VALUES ('cool', '2021-03-01 20:47:30.810000', '2021-03-01 20:47:30.836808', false);
INSERT INTO public.m_keywords (name, created, changed, is_deleted) VALUES ('Long', '2021-03-01 21:54:35.408000', '2021-03-01 21:54:35.432410', false);
INSERT INTO public.m_keywords (name, created, changed, is_deleted) VALUES ('coooool', '2021-03-02 00:01:39.718000', '2021-03-02 00:01:39.745440', false);
INSERT INTO public.m_keywords (name, created, changed, is_deleted) VALUES ('parampampam', '2021-03-02 00:02:01.624000', '2021-03-02 00:02:01.626646', false);

INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('ART', '2021-02-23 19:42:14.483000', '2021-02-23 19:42:14.486028', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('ABSTRACTION', '2021-02-23 19:41:36.027000', '2021-02-23 19:41:36.059176', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('HOLIDAYS', '2021-02-23 19:42:26.051000', '2021-02-23 19:42:26.063843', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('BEAUTY_AND_FASHION', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('BACKGROUNDS_AND_TEXTURES', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('MEDICINE', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('BUILDINGS_AND_ATTRACTIONS', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('NATURE', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('PEOPLE', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('ITEMS', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('SPACE', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);
INSERT INTO public.m_category (name, created, changed, is_deleted) VALUES ('TRANSPORT', '2021-03-05 14:35:42.903596', '2021-03-05 14:35:42.903596', false);



V1_2__database_dump.sql