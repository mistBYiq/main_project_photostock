create or replace function func_keywords_search(id bigint) returns SETOF m_keywords
    language sql
as $$
select k.*
from m_keywords k
         join l_keyword_photo lkp
              on k.id = lkp.keyword_id and lkp.photo_id = $1;

$$;

alter function func_keywords_search(bigint) owner to postgres;

create or replace function func_photos_search_by_license_type(name character varying) returns SETOF m_photos
    language sql
as $$
select p.*
from m_photos p
         join m_license l
              on p.id = l.photo_id and l.name = $1
where p.is_deleted = false and p.is_new = false;
$$;

alter function func_photos_search_by_license_type(varchar) owner to postgres;

create or replace function func_photos_search_by_category_name(name character varying) returns SETOF m_photos
    language sql
as $$
select p.*
from m_photos p
         join m_category c
              on p.id = c.photo_id and c.name = $1
where p.is_deleted = false and p.is_new = false;
$$;

alter function func_photos_search_by_category_name(varchar) owner to postgres;

create or replace function func_photos_search_by_keyword_name(name character varying) returns SETOF m_photos
    language sql
as $$
select p.*
from m_photos p
         join l_keyword_photo lkp
              on p.id = lkp.photo_id and lkp.keyword_id = (select k.id from m_keywords k where k.name = $1)
where p.is_new = false and p.is_deleted = false;

$$;

alter function func_photos_search_by_keyword_name(varchar) owner to postgres;

