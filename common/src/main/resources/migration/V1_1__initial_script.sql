create schema if not exists public;

create table if not exists m_users
(
    id bigserial not null
        constraint m_users_pk
            primary key,
    name varchar(50) not null,
    surname varchar(50) not null,
    date_birth date,
    gender varchar(15) default 'NOT_SELECTED'::character varying,
    phone varchar(50),
    email varchar(150) not null,
    password varchar(100) not null,
    login varchar(50) not null,
    country varchar(100),
    city varchar(100),
    street varchar(100),
    postal_code varchar(30),
    created timestamp default CURRENT_TIMESTAMP not null,
    changed timestamp default CURRENT_TIMESTAMP not null,
    is_deleted boolean default false not null,
    status varchar(20) default 'ACTIVE'::character varying not null,
    balance integer default 0 not null
);

alter table m_users owner to postgres;

create unique index if not exists m_users_email_uindex
    on m_users (email);

create unique index if not exists m_users_login_uindex
    on m_users (login);

create index if not exists m_users_email_index
    on m_users (email);


create table if not exists m_roles
(
    id bigserial not null
        constraint roles_pkey
            primary key,
    name varchar(50) not null,
    user_id bigint not null
        constraint roles_m_users_id_fk
            references m_users
            on update cascade on delete cascade
);

alter table m_roles owner to postgres;

create unique index if not exists m_roles_user_id_uindex
    on m_roles (user_id);

create index if not exists m_roles_name_index
    on m_roles (name);


create table if not exists m_keywords
(
    id bigserial not null
        constraint keywords_pkey
            primary key,
    name varchar(30) not null,
    created timestamp with time zone default CURRENT_TIMESTAMP not null,
    changed timestamp with time zone default CURRENT_TIMESTAMP not null,
    is_deleted boolean default false not null
);

alter table m_keywords owner to postgres;

create unique index if not exists m_keywords_name_uindex
    on m_keywords (name);

create index if not exists m_keywords_name_index
    on m_keywords (name);


create table if not exists m_photos
(
    id bigserial not null
        constraint m_photos_pkey
            primary key,
    name varchar(50) not null,
    author_id bigint
        constraint m_photos_m_users_id_fk
            references m_users
            on update cascade on delete cascade,
    url varchar(200),
    preview varchar(200),
    created timestamp with time zone default CURRENT_TIMESTAMP not null,
    changed timestamp with time zone default CURRENT_TIMESTAMP not null,
    is_deleted boolean default false not null,
    is_new boolean default true not null,
    description varchar(200)
);

alter table m_photos owner to postgres;

create index if not exists m_photos_name_index
    on m_photos (name);


create table if not exists m_category
(
    id bigserial not null
        constraint category_pkey
            primary key,
    name varchar(100),
    created timestamp with time zone default CURRENT_TIMESTAMP not null,
    changed timestamp with time zone default CURRENT_TIMESTAMP not null,
    is_deleted boolean default false not null,
    photo_id bigint not null
        constraint m_category_m_photos_id_fk
            references m_photos
            on update cascade on delete cascade
);

alter table m_category owner to postgres;

create index if not exists m_category_name_index
    on m_category (name);

create unique index if not exists m_category_photo_id_uindex
    on m_category (photo_id);


create table if not exists m_license
(
    id bigserial not null
        constraint m_licence_pkey
            primary key,
    name varchar(20) not null,
    price integer default 0 not null,
    photo_id bigint
        constraint m_license_m_photos_id_fk
            references m_photos
            on update cascade on delete cascade,
    created timestamp with time zone default CURRENT_TIMESTAMP not null,
    changed timestamp with time zone default CURRENT_TIMESTAMP not null,
    is_deleted boolean default false not null
);

alter table m_license owner to postgres;

create index if not exists m_license_name_index
    on m_license (name);

create table if not exists orders
(
    id bigserial not null
        constraint orders_pkey
            primary key,
    user_id bigint not null
        constraint orders_m_users_id_fk
            references m_users
            on update cascade,
    total_price integer default 0,
    created timestamp with time zone default CURRENT_TIMESTAMP not null,
    changed timestamp with time zone default CURRENT_TIMESTAMP not null,
    is_deleted boolean default false not null,
    status varchar(20) default 'NOT_PAID'::character varying,
    photo_id bigint not null
        constraint orders_m_photos_id_fk
            references m_photos,
    license_id bigint not null
        constraint orders_m_license_id_fk
            references m_license,
    author_id bigint not null
        constraint orders_m_users_id_fk_2
            references m_users
);

alter table orders owner to postgres;

create index if not exists orders_user_id_index
    on orders (user_id);


create table if not exists l_keyword_photo
(
    id bigserial not null
        constraint l_keyword_photo_pk
            primary key,
    keyword_id bigint
        constraint l_keyword_photo_m_keywords_id_fk
            references m_keywords,
    photo_id bigint
        constraint l_keyword_photo_m_photos_id_fk
            references m_photos
            on update cascade on delete cascade
);

alter table l_keyword_photo owner to postgres;

create unique index if not exists l_keyword_photo_id_uindex
    on l_keyword_photo (id);

